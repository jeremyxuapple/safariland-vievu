<?php
namespace MiniBC\addons\sladministration\objects;

class Account
{
	public $id;
	public $name;
	public $store;
	public $agency;
	public $adminEmail;
	public $username;
	public $password;
}