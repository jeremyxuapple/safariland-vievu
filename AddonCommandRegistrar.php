<?php
namespace MiniBC\addons\sladministration;

use MiniBC\addons\sladministration\commands\UpdateAccountStatus;
use MiniBC\core\interfaces\CommandRegistrarInterface;
use Symfony\Component\Console\Application;

class AddonCommandRegistrar implements CommandRegistrarInterface
{
	/**
	 * register CLI commands
	 *
	 * @param Application $application
	 */
	public function registerCommands(Application $application)
	{
		// update account status from RM-API
		$application->add(new UpdateAccountStatus());
	}
}