/**
 * Safariland Checkout Form
 *
 * @external {Object} jQuery
 * @external MINIBC
 */
if (!MINIBC) var MINIBC = {};

(function($, mbc) {
	var CheckoutForm = function() {
		var modal,
			log,
			data = {},
			self = this,
			isSaving = false;

		var apiBasePath = '/apps/sladministration/storefront';
		var nameGenerator = new mbc.Safariland.AccountNameGenerator();
		var state = { 'currentStep': 'step-one' };
		var cartId = '';

		var PHONE_MIN_LENGTH = 10;
		var SIGNATURE_MIN_LENGTH = 5;
		var PASSWORD_MIN_LENGTH = 8;

		var steps = [
			'step-one',
			'step-two',
			'step-three',
			'step-four',
			'step-five'
		];

		/**
		 * reset state
		 */
		var resetData = function() {
			log.debug('Resetting registration form data.');

			data = {
				'entity': '',
				'usage': '',
				'agency': {
					'agency_name': '',
					'abbreviation': '',
					'address_1': '',
					'address_2': '',
					'city': '',
					'state': '',
					'zip': '',
					'account_name': ''
				},
				'contact': {
					'first_name': '',
					'last_name': '',
					'phone': '',
					'role': ''
				},
				'admin': {
					'first_name': '',
					'last_name': '',
					'phone': '',
					'role': ''
				},
				'security': {
					'admin_email': '',
					'username': '',
					'password': '',
					'question': '',
					'answer': ''
				}
			};

			state.currentStep = 0;
		};

		var resetState = function() {
			log.debug('Resetting registration form state.');
			state.currentStep = 'step-one';
		};

		var commitData = function() {
			return mbc.request(data, apiBasePath + '/registration/save', 'POST' ,'json');
		};

		var changeStep = function(stepIndex) {
			log.debug('Changing registration form to step ' + stepIndex + '.');

			if (stepIndex > -1 && stepIndex <= steps.length) {
				var newStep = steps[stepIndex];
				var newStepSelector = '.' + newStep;

				// assign new step
				state.currentStep = newStep;

				if (stepIndex === 3) {
					updateAgreementForm($(newStepSelector + ' .agreement-wrapper'));
				}

				// update nav display
				$('.steps-nav .step-number', modal).text(stepIndex + 1);
				$('.steps-nav .steps-nav-item:not(' + newStepSelector + ')', modal).removeClass('step-active');
				$('.steps-nav .steps-nav-item' + newStepSelector, modal).addClass('step-active');

				$('.step:visible', modal).hide();
				$(newStepSelector).show();
			}
		};

		var validateStepOne = function(step, button) {
			var governmentEntities = [ 'federal', 'local', 'state', 'tribal' ];
			var entity = $('input[name="agreement-entity"]:checked', step).val();

			if (typeof entity !== 'undefined') {
				// save data
				data.entity = entity;
				data.usage = (governmentEntities.indexOf(entity) > -1) ? 'government' : 'public';

				var message = '';

				if (data.usage === 'public') {
					message = 'At this time, all online orders for PRO subscriptions are limited to government entities.<br />We are happy to assist non-government customers with subscriptions, through our LE5 Customer Service team at 888-285-4548.';
				}

				if (data.usage === 'government' && button.is('.no')) {
					message = 'The PRO software legal agreement will be presented within the purchase. The digital signor for the PRO agreement must be an authorized person and have signing/purchasing authority designated through their agency. Complete purchase with the authorized person present or call the LE5 Customer Service team for assistance at 888-285-4548';
				}

				if (message !== '') {
					// insert error message and replace button
					var error = $('<div class="Message ErrorMessage" />').html(message);
					$('.actions', step).prev('hr').remove();
					error.insertBefore($('.actions', step));
					$('.actions', step).html('<a href="#" class="button button-close">Close</a>');

					return false;
				}

				return true;
			}

			log.error('Validation for step 1 of the registration form failed.');

			return false;
		};

		var validateStepTwo = function(step) {
			var i, formElement, formValue, error = false;

			var agencyFields = [
				{ 'form': 'agency-name', 'property': 'agency_name', 'required': true },
				{ 'form': 'agency-abbr', 'property': 'abbreviation', 'required': true },
				{ 'form': 'agency-address-1', 'property': 'address_1', 'required': true },
				{ 'form': 'agency-address-2', 'property': 'address_2', 'required': false },
				{ 'form': 'agency-city', 'property': 'city', 'required': true },
				{ 'form': 'agency-zip', 'property': 'zip', 'required': true },
				{ 'form': 'agency-state', 'property': 'state', 'required': true },
				{ 'form': 'agency-acct', 'property': 'account_name', 'required': true }
			];

			var contactFields = [
				{ 'form': 'contact-fname', 'property': 'first_name', 'required': true },
				{ 'form': 'contact-lname', 'property': 'last_name', 'required': true },
				{ 'form': 'contact-phone', 'property': 'phone', 'required': true },
				{ 'form': 'contact-role', 'property': 'role', 'required': true }
			];

			var value, validateFields = agencyFields.concat(contactFields);

			for (i = 0; i < validateFields.length; i++) {
				formElement = $('[name=' + validateFields[i]['form'] + ']', step);

				if (validateFields[i]['required']) {
					if (formElement.val() === '') {
						formElement.addClass('input-error');
						error = true;
						break;
					}

					if (validateFields[i]['property'] === 'zip') {
						value = formElement.val();

						if (!value.match(/\d{5}/g)) {
							formElement.addClass('input-error');
							error = true;
							break;
						}
					}

					if (validateFields[i]['property'] === 'phone') {
						value = formElement.val();

						if (value.length < PHONE_MIN_LENGTH) {
							formElement.addClass('input-error');

							error = true;
							break;
						}
					}
				}

				formElement.removeClass('input-error');
			}

			if (error) {
				log.error('Validation for step 2 of the registration form failed.');

				return false;
			}

			// save data
			var agencyData = data.agency, contactData = data.contact;

			for (i = 0; i < agencyFields.length; i++) {
				formElement = $('[name=' + agencyFields[i]['form'] + ']');
				formValue = formElement.val();

				agencyData[agencyFields[i]['property']] = formValue;
			}

			for (i = 0; i < contactFields.length; i++) {
				formElement = $('[name=' + contactFields[i]['form'] + ']');
				formValue = formElement.val();

				contactData[contactFields[i]['property']] = formValue;
			}

			data.agency = agencyData;
			data.contact = contactData;

			return true;
		};

		var validateStepThree = function(step) {
			var i, formElement, formValue, error = false;

			var adminFields = [
				{ 'form': 'admin-fname', 'property': 'first_name', 'required': true },
				{ 'form': 'admin-lname', 'property': 'last_name', 'required': true },
				{ 'form': 'admin-phone', 'property': 'phone', 'required': true },
				{ 'form': 'admin-role', 'property': 'role', 'required': true }
			];

			var securityFields = [
				{ 'form': 'admin-email', 'property': 'admin_email', 'required': true },
				{ 'form': 'admin-username', 'property': 'username', 'required': true },
				{ 'form': 'admin-password', 'property': 'password', 'required': true },
				{ 'form': 'admin-question', 'property': 'question', 'required': true },
				{ 'form': 'admin-answer', 'property': 'answer', 'required': true }
			];

			var validateFields = adminFields.concat(securityFields);

			for (i = 0; i < validateFields.length; i++) {
				formElement = $('[name=' + validateFields[i]['form'] + ']', step);

				if (validateFields[i]['required']) {
					if (formElement.val() === '') {
						formElement.addClass('input-error');
						error = true;
						break;
					}
				}

				var value = formElement.val();

				if (validateFields[i]['property'] === 'password') {
					var verifyPassword = $('[name=admin-verify]').val();

					if (!verifyPassword || verifyPassword === '' || verifyPassword !== value) {
						formElement.addClass('input-error');
						error = true;
						break;
					}

					if (value.length < PASSWORD_MIN_LENGTH) {
						formElement.addClass('input-error');
						error = true;
						break;
					}
				}

				if (validateFields[i]['property'] === 'phone') {
					if (value.length < PHONE_MIN_LENGTH) {
						formElement.addClass('input-error');
						error = true;
						break;
					}
				}

				if (validateFields[i]['property'] === 'admin_email') {
					if (!value.match(/@[a-zA-Z]+\.[a-zA-Z]+$/g)) {
						formElement.addClass('input-error');
						error = true;
						break;
					}
				}

				formElement.removeClass('input-error');
			}

			if (error) {
				log.error('Validation for step 3 of the registration form failed.');

				return false;
			}

			// save data
			var adminData = data.admin, securityData = data.security;

			for (i = 0; i < adminFields.length; i++) {
				formElement = $('[name=' + adminFields[i]['form'] + ']');
				formValue = formElement.val();

				adminData[adminFields[i]['property']] = formValue;
			}

			for (i = 0; i < securityFields.length; i++) {
				formElement = $('[name=' + securityFields[i]['form'] + ']');
				formValue = formElement.val();

				securityData[securityFields[i]['property']] = formValue;
			}

			data.admin = adminData;
			data.security = securityData;

			return true;
		};

		var validateStepFour = function(step) {
			var initials = $('[name="accept-initial"]', step).val();

			if (typeof initials !== 'undefined' && initials !== '' && initials.length >= SIGNATURE_MIN_LENGTH) {
				return true;
			}

			log.error('Validation for step 4 of the registration form failed.');

			return false;
		};

		var validateStep = function(stepIndex, step, button) {
			if (stepIndex === 0) {
				return validateStepOne(step, button);
			} else if (stepIndex === 1) {
				return validateStepTwo(step);
			} else if (stepIndex === 2) {
				return validateStepThree(step);
			} else if (stepIndex === 3) {
				return validateStepFour(step);
			}
		};

		var getPrevStepIndex = function() {
			var currentStepIndex = steps.indexOf(state.currentStep);

			if (currentStepIndex < 1) {
				return -1;
			}

			return currentStepIndex - 1;
		};

		var getNextStepIndex = function() {
			var currentStepIndex = steps.indexOf(state.currentStep);

			if (currentStepIndex < 0 || isLastStep()) {
				return -1;
			}

			return currentStepIndex + 1;
		};

		var isLastStep = function() {
			var currentStepIndex = steps.indexOf(state.currentStep);

			return (currentStepIndex === (steps.length - 1));
		};

		var enableStepOneActions = function() {
			$('.step.step-one .actions a').removeClass('disabled');
		};

		var enableStepFourActions = function() {
			// check number of characters in digital signature
			var signature = $('.step-four .accept-agreement input[type=text]').val();

			if (signature.length < 5) {
				$('.step-four .actions a.button-next').addClass('disabled');
			} else {
				$('.step-four .actions a.button-next').removeClass('disabled');
			}

			return true;
		};

		var disableStepFourActions = function() {
			$('.step-four .actions a.button-next').addClass('disabled');

		};

		var enableDigitalSignature = function() {
			$('.step-four .accept-agreement input[type=text]').removeAttr('disabled');
		};

		var disableDigitalSignature = function() {
			$('.step-four .accept-initial input[type=text]').attr('disabled', true);
		};

		var updateAgreementForm = function(form) {
			// date
			var month = [
				'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
			];

			var date = new Date();
			var dateStr = month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();

			var customerName = data.admin.first_name + ' ' + data.admin.last_name;

			var agencyAddress = data.agency.address_1;
			if (data.agency.address_2 && data.agency.address_2 !== '') {
				agencyAddress += ' ' + data.agency.address_2;
			}

			// entity type
			var entityName = '';
			if (data.entity === 'federal') entityName = 'United States Federal Government agency';
			else if (data.entity === 'local') entityName = 'Local Government agency';
			else if (data.entity === 'state') entityName = 'State Government agency';
			else entityName = 'Tribal Government agency';

			$('.current-date', form).text(dateStr);
			$('.full-name', form).text(customerName);
			$('.contact-name', form).text(data.contact.first_name + ' ' + data.contact.last_name);
			$('.contact-role', form).text(data.contact.role);
			$('.contact-email', form).text(data.security.admin_email);
			$('.contact-phone', form).text(data.contact.phone);
			$('.street_address', form).text(agencyAddress);
			$('.street_city', form).text(data.agency.city);
			$('.street_state', form).text(data.agency.state);
			$('.street_zip', form).text(data.agency.zip);
			$('.agency-entity', form).text(entityName);
		};

		var updateAccountName = function() {
			var legalName = $('#agency-name').val();
			var state = $('#agency-state').val();
			var accountName = nameGenerator.generate(legalName, state);

			$('#agency-acct').val(accountName);
		};

		var savePendingAccount = function(accountId, cartId) {
			document.cookie = 'SL_PENDING_ID=' + accountId;
			document.cookie = 'SL_PENDING_CART=' + cartId;
		};

		var getPendingAccount = function() {
			var accountId = get_cookie('SL_PENDING_ID');
			var cartId = get_cookie('SL_PENDING_CART');

			return { 'account_id': accountId, 'cart_id': cartId };
		};

		var showLoadingIndicator = function(button) {
			var buttonText = $(button).text();
			$(button)
				.attr('data-button-text', buttonText)
				.text('Please Wait...');
			$('<span class="please-wait-text" />').text('This may take a minute').insertAfter(button);
		};

		var hideLoadingIndicator = function(button) {
			var buttonText = $(button).attr('data-button-text');
			button.text(buttonText);
			$('.please-wait-text').remove();
		};

		var bindEvents = function() {
			modal
				.on('click', '.button-next', function(e) {
					e.preventDefault();

					if ($(this).is('.disabled')) {
						return false;
					}

					var stepIndex = steps.indexOf(state.currentStep);
					var step = $(this).parents('.step');

					if (!validateStep(stepIndex, step, $(this))) {
						// show error
						return false;
					}

					// advance to next step
					var nextStep = getNextStepIndex();

					/**
					 * alert box when clicking on grey'd out proceed button
					 */

					if (nextStep > -1) {
						if (nextStep === 4) {
							var button = $(this);
							isSaving = true;

							showLoadingIndicator(button);
							commitData()
								.done(function(resp) {
									if (resp.success === true) {
										savePendingAccount(resp.account_id, cartId);
										changeStep(nextStep);
									} else {
										// show error
										log.error('Failed to create account: ' + resp.error);
										alert(resp.error);
									}
								})
								.always(function() {
									isSaving = false;
									hideLoadingIndicator(button);
								});
						} else {
							changeStep(nextStep);
						}
					}

					return false;
				})
				.on('click', '.button-prev', function(e) {
					e.preventDefault();

					var prevStep = getPrevStepIndex();

					if (prevStep > -1) {
						changeStep(prevStep);
					}

					return false;
				})
				.on('click', '.button-cancel', function(e) {
					e.preventDefault();

					var stepIndex = steps.indexOf(state.currentStep);
					var step = $(this).parents('.step');

					if (!validateStep(stepIndex, step, $(this))) {
						// show error
						return false;
					}

					return false;
				})
				.on('click', '.button-close', function(e) {
					e.preventDefault();

					window.location.replace('/faq');

					return false;
				})
				.on('click', '.button-agree', function(e) {
					e.preventDefault();

					// enable digital signature
					enableDigitalSignature();

					return false;
				})
				.on('click', '.button-checkout', function(e) {
					e.preventDefault();

					self.hideModal();
					showPageContent();

					return false;
				})
				.on('keyup', '#agency-name', updateAccountName)
				.on('change', '#agency-state', updateAccountName)
				.on('change', 'input[name=agreement-entity]', enableStepOneActions)
				.on('keyup', '#accept-initial', enableStepFourActions);
		};

		var loadModal = function() {
			return mbc.request({}, apiBasePath + '/registration/load', 'GET', 'html')
				.done(function(html) {
					$(html).appendTo('body');

					log.debug('Registration form appended to body element.');
				})
				.fail(function() {
					log.error('Failed to load registration form template.');
				});
		};

		var loadStylesheet = function() {
			return mbc.request({}, apiBasePath + '/css', 'GET', 'text')
				.done(function(css) {
					$('<style />')
						.attr('type', 'text/css')
						.html(css)
						.appendTo('head');

					log.debug('Storefront stylesheet appended to head element.');
				})
				.fail(function() {
					log.error('Failed to load storefront stylesheet.');
				});
		};

		var hidePageContent = function() {
			log.debug('Hiding page content.');
			$('.page').fadeOut();
		};

		var showPageContent = function() {
			log.debug('Showing page content.');
			$('.page').fadeIn();
		};

		var getCartContent = function() {
			var progress = new $.Deferred();

			log.debug('Retrieving cart details from storefront api.');

			$.ajax(
				{
					url: '/api/storefront/carts',
					headers: { 'Accept': 'application/json' },
					dataType: 'json'
				}
			)
				.done(function(resp) {
					return progress.resolve(resp[0]);
				})
				.fail(progress.reject);

			return progress.promise();
		};

		var validateCart = function(cart) {
			var lineItems = cart.lineItems;
			var requestData = { 'products': [] };

			log.debug('Checking cart content for recurring products.');

			$.each(lineItems.digitalItems, function(i, item) {
				requestData.products.push(item.productId);
			});

			$.each(lineItems.physicalItems, function(i, item) {
				requestData.products.push(item.productId);
			});

			return mbc.request(requestData, apiBasePath + '/registration/cart/validate', 'POST', 'json');
		};

		this.showModal = function() {
			log.debug('Displaying modal.');
			modal.fadeIn();
		};

		this.hideModal = function() {
			log.debug('Hiding modal.');
			modal.fadeOut();
		};

		/**
		 * initialize checkout form
		 */
		this.init = function(logInstance) {
			log = logInstance;
			log.debug('Initializing checkout form.');

			var isNewSession = (window.location.pathname === '/checkout.php' && window.location.search === '');

			getCartContent()
				.done(function(cart) {
					cartId = cart.id;

					var pendingAccount = getPendingAccount();

					if (pendingAccount && !isNewSession) {
						if (pendingAccount.cart_id === cartId) {
							log.debug('Existing pending registration found, continuing checkout.');
							return;
						}
					}
					
					// check if cart contains recurring product
					validateCart(cart)
						.done(function(resp) {
							if (resp.register !== true) {
								return;
							}

							hidePageContent();

							$.when(loadStylesheet(), loadModal())
								.done(function() {
									modal = $('#myModal');

									resetData();
									resetState();
									bindEvents();

									// display the first step
									changeStep(0);

									// display modal
									self.showModal();
								});
						})
						.fail(function() {
							log.error('Failed to validate cart content.');
						});
				})
				.fail(function() {
					log.error('Failed to get cart content from storefront api.');
				});
		};
	};

	/**
	 * assign to namespace
	 */
	if (!mbc.Safariland) {
		mbc.Safariland = {};
	}

	mbc.Safariland.CheckoutForm = CheckoutForm;
})(window.jQuery, MINIBC);