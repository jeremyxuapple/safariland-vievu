if (!MINIBC) MINIBC = {};

(function($, mbc) {
	var CustomerAdmin = function() {
		var log, wrapper, self = this;
		adminData = null;
		var apiBasePath = '/apps/sladministration/storefront';


		var setupMenuLinks = function() {
			// insert menu links
			var menuWrapper = $('.account-header .account-toolbar');
			menuWrapper.append('<a href="#account" class="account-tool active" data-section="account">Account Details</a>');
			menuWrapper.append('<a href="#sladmin" class="account-tool" data-section="sladmin">Administration Details</a>');

			$('head').append('<style type="text/css">.account-tool.active{color:black;font-weight:bold}</style>');
			$('head').append('<style type="text/css">input[readonly]{background-color:#f2f2f2}</style>');

			$(menuWrapper).on('click', '.account-tool:not(.active)', function(e) {
				e.preventDefault();

				// switch active class
				$('.account-tool.active').removeClass('active');
				$(this).addClass('active');
				showSection($(this).attr('data-section'));

				return false;
			});

			
		};
		var loadStylesheet = function() {
			return mbc.request({}, apiBasePath + '/css', 'GET', 'text')
				.done(function(css) {
					$('<style />')
						.attr('type', 'text/css')
						.html(css)
						.appendTo('head');

					log.debug('Storefront stylesheet appended to head element.');
				})
				.fail(function() {
					log.error('Failed to load storefront stylesheet.');
				});
		};
		var getAdminData = function() {
            var customerEmail = $("input[data-field-type='EmailAddress']").val();
            // console.log("Customer email is:",customerEmail);
            return mbc.request({'email':customerEmail}, "/apps/sladministration/storefront/account/data", "GET", "html")
        };
		var getAdminTemplate = function() {
			return mbc.request({}, '/apps/sladministration/storefront/account/template', 'GET', 'html');
		};
		var setupAdminData = function(resp) {
			// console.log("setupAdminData");
			adminData=jQuery.parseJSON(resp);
		};

		var setupAdminDisplay = function(html) {
			// console.log("setupAdminDisplay");

			var adminWrapper = $('<div id="sladmin" class="content-section hidden" style="display:none" />');
			adminWrapper.append(html).appendTo(wrapper);

			$("#personnel_popup_form").hide();

			//Populate the template with data values:
			// console.log("Admin data:",adminData);
			//Agency//
			$('input[name="agency-name"]').val(adminData.agency_account.agency_name);
			$('input[name="agency-abbr"]').val(adminData.agency_account.abbreviation);
			$('input[name="agency-street_1"]').val(adminData.agency_account.address_1);
			$('input[name="agency-street_2"]').val(adminData.agency_account.address_2);
			$('input[name="agency-city"]').val(adminData.agency_account.city);
			$('input[name="agency-zip"]').val(adminData.agency_account.zip);

			$('select[name="agency-state"]').val(adminData.agency_account.state);
			$('input[name="agency-account_name"]').val(adminData.agency_account.account_name);
			//Primary Contact//
			$('input[name="contact-first_name"]').val(adminData.primary_contact.first_name);
			$('input[name="contact-last_name"]').val(adminData.primary_contact.last_name);
			$('input[name="contact-phone"]').val(adminData.primary_contact.phone);
			$('input[name="contact-role"]').val(adminData.primary_contact.role);
			//Admin Contact//
			$('input[name="admin-first_name"]').val(adminData.admin_contact.first_name);
			$('input[name="admin-last_name"]').val(adminData.admin_contact.last_name);
			$('input[name="admin-phone"]').val(adminData.admin_contact.phone);
			$('input[name="admin-role"]').val(adminData.admin_contact.role);
			//Account Admin//
			$('input[name="account-admin_email"]').val(adminData.agency_account.admin_email);			
			$('input[name="account-username"]').val(adminData.agency_account.username);
			$('option[value="'+adminData.agency_account.question+'"]').prop('selected',true);//(adminData.agency_account.state);
			$('input[name="account-answer"]').val(adminData.agency_account.answer);

			if (adminData.account_users.length>=1){
				// console.log("adding account users..");
				$('.authorized-contact .no-users').remove();
				jQuery.each(adminData.account_users, function(index,account_user){
					// console.log("looping account users..", index);
					$("<p>" + account_user.first_name + " " + account_user.last_name + " - " + account_user.phone + " - " + account_user.email + " -  " + account_user.role + "</p> <!--<button personnel-id=" + account_user.id +" class='edit_personnel' class='button account-button-primary'>[EDIT]</button>-->").insertAfter(".authorized-contact h2");
					// $('.authorized-contact').prepend("<p>" + account_user.first_name + " " + account_user.last_name + " - " + account_user.phone + " - " + account_user.email + " -  " + account_user.role + "</p> <!--<button personnel-id=" + account_user.id +" class='edit_personnel' class='button account-button-primary'>[EDIT]</button>-->");
				});
			}
			//
			
			$('#sladmin').on('click', '#admin_update_details', function(e) {
				// console.log("Submitting changes to MiniBC...");
				var agency_account_agency_name = $('input[name="agency-name"]').val();
				var agency_account_abbreviation = $('input[name="agency-abbr"]').val();
				var agency_account_address_1 = $('input[name="agency-street_1"]').val();
				var agency_account_address_2 = $('input[name="agency-street_2"]').val();
				var agency_account_city = $('input[name="agency-city"]').val();
				var agency_account_zip = $('input[name="agency-zip"]').val();
				var agency_account_state = $('select[name="agency-state"]').val();
				var agency_account_account_name = $('input[name="agency-account_name"]').val();
				var primary_contact_first_name = $('input[name="contact-first_name"]').val();
				var primary_contact_last_name = $('input[name="contact-last_name"]').val();
				var primary_contact_phone = $('input[name="contact-phone"]').val();
				var primary_contact_role = $('input[name="contact-role"]').val();
				var admin_contact_first_name = $('input[name="admin-first_name"]').val();
				var admin_contact_last_name = $('input[name="admin-last_name"]').val();
				var admin_contact_phone = $('input[name="admin-phone"]').val();
				var admin_contact_role = $('input[name="admin-role"]').val();
				var agency_account_admin_email = $('input[name="account-admin_email"]').val();
				var agency_account_username = $('input[name="account-username"]').val();
				var agency_account_question = $('select[name="account-question"]').val();
				var agency_account_answer = $('input[name="account-answer"]').val();
         		mbc.request({'agency_account_id':adminData.agency_account.id,
         					'agency_account_agency_name':agency_account_agency_name,
							'agency_account_abbreviation':agency_account_abbreviation,
							'agency_account_address_1':agency_account_address_1,
							'agency_account_address_2':agency_account_address_2,
							'agency_account_city':agency_account_city,
							'agency_account_zip':agency_account_zip,
							'agency_account_state':agency_account_state,
							'agency_account_account_name':agency_account_account_name,
							'primary_contact_first_name':primary_contact_first_name,
							'primary_contact_last_name':primary_contact_last_name,
							'primary_contact_phone':primary_contact_phone,
							'primary_contact_role':primary_contact_role,
							'admin_contact_first_name':admin_contact_first_name,
							'admin_contact_last_name':admin_contact_last_name,
							'admin_contact_phone':admin_contact_phone,
							'admin_contact_role':admin_contact_role,
							'agency_account_admin_email':agency_account_admin_email,
							'agency_account_username':agency_account_username,
							'agency_account_question':agency_account_question,
							'agency_account_answer':agency_account_answer,

							'admin_contact_id': adminData.agency_account.admin_contact_id,
							'primary_contact_id': adminData.agency_account.primary_contact_id
         		}, "/apps/sladministration/storefront/account/data", "POST", "json").pipe(function(resp){
         			// console.log("MiniBC update complete.");
         			$('.account-content .alert').remove();
         			if (resp.success==1){         				
         				$('.account-content').prepend('<div class="alert alert-success">\
													  <div class="alert-message">\
													      Your account details have been updated.\
													  </div>\
													</div>');  
         			}else{
         				$('.account-content').prepend('<div class="alert alert-error">\
						  <div class="alert-message">\
						      Your account details could not be updated. ' + resp.message +
						  '</div>\
						</div>');  
         			}
         			var $container = $("html,body");
					var $scrollTo = $('.account-content');
					$container.animate({scrollTop: 0},300);
         		});

				e.preventDefault();
				return false;
			});


			$('#sladmin').on('click', '#admin_add_personnel', function(e) {
					// console.log("Show personnel UI...");
					$("#personnel_popup_form").fadeIn(300);
					e.preventDefault();
					return false;
			});
			$('#sladmin').on('click', '#admin_hide_personnel', function(e) {
					// console.log("Hide personnel UI...");
					$("#personnel_popup_form").fadeOut(300);
					$("#personnel_popup_form .alert").remove();
					e.preventDefault();
					return false;
			});
			$('#sladmin').on('click', '#admin_view_agreement', function(e) {
					// console.log("Show personnel UI...");
					updateAgreementForm();					
					$("#agreement_popup_form").fadeIn(300);
					e.preventDefault();
					return false;
			});
			$('#sladmin').on('click', '.admin_hide_agreement', function(e) {
					// console.log("Show personnel UI...");
					$("#agreement_popup_form").fadeOut(300);
					e.preventDefault();
					return false;
			});

			$('#sladmin').on('click', '#add-contact-button', function(e) {
					// console.log("Sending contact request to MiniBC...");
					var user_first_name = $("input[name='addcontact-first_name']").val();
					var user_last_name = $("input[name='addcontact-last_name']").val();
					var user_phone = $("input[name='addcontact-phone']").val();
					var user_email = $("input[name='addcontact-email']").val();
					var user_role = $("input[name='addcontact-role']").val();
					var success=true;
					if (user_first_name==''){
						alert("Please enter a first name.");
						success=false;
					}
					else if (user_last_name==''){
						alert("Please enter a last name.");
						success=false;
					}
					else if (user_phone==''){
						alert("Please enter a phone number.");
						success=false;
					}
					else if (user_email==''){
						alert("Please enter an email.");
						success=false;
					}
					else if (user_role==''){
						alert("Please enter a role.");
						success=false;
					}
					else if (success){
						mbc.request({'agency_account_id':adminData.agency_account.id,
						 			 'user_first_name':user_first_name,
									 'user_last_name':user_last_name,
									 'user_email':user_email,
									 'user_phone':user_phone,
									 'user_role':user_role
	         			}, "/apps/sladministration/storefront/account/adduser", "POST", "json").pipe(addUserToDisplay);
					}
					e.preventDefault();
					return false;
			});
		};
		var updateAgreementForm = function() {
			// date
			var form = $("#agreement_popup_form");
			var data = {
				'entity': '',
				'usage': '',
				'agency': {
					'agency_name': $("input[name='agency-name']").val(),
					'abbreviation': $("input[name='agency-abbr']").val(),
					'address_1': $("input[name='agency-street_1']").val(),
					'address_2': $("input[name='agency-street_2  ']").val(),
					'city': $("input[name='agency-city']").val(),
					'state': $("select[name='agency-state']").val(),
					'zip': $("input[name='agency-zip']").val(),
					'account_name': $("input[name='agency-account_name']").val()
				},
				'contact': {
					'first_name': $("input[name='contact-first_name']").val(),
					'last_name': $("input[name='contact-last_name']").val(),
					'phone': $("input[name='contact-phone']").val(),
					'role': $("input[name='contact-role']").val()
				},
				'admin': {
					'first_name': $("input[name='admin-first_name']").val(),
					'last_name': $("input[name='admin-last_name']").val(),
					'phone':  $("input[name='admin-phone']").val(),
					'role':  $("input[name='admin-role']").val()
				},
				'security': {
					'admin_email': $("input[name='account-admin_email']").val()
				}
			};
			var month = [
				'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
			];

			var date = new Date();
			var dateStr = month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();

			var customerName = data.admin.first_name + ' ' + data.admin.last_name;

			var agencyAddress = data.agency.address_1;
			if (data.agency.address_2 && data.agency.address_2 !== '') {
				agencyAddress += ' ' + data.agency.address_2;
			}

			// entity type
			var entityName = '';
			if (data.entity === 'federal') entityName = 'United States Federal Government agency';
			else if (data.entity === 'local') entityName = 'Local Government agency';
			else if (data.entity === 'state') entityName = 'State Government agency';
			else entityName = 'Tribal Government agency';
			

			$('.current-date', form).text(dateStr);
			$('.full-name', form).text(customerName);
			$('.contact-name', form).text(data.contact.first_name + ' ' + data.contact.last_name);
			$('.contact-role', form).text(data.contact.role);
			$('.contact-email', form).text(data.security.admin_email);
			$('.contact-phone', form).text(data.contact.phone);
			$('.street_address', form).text(agencyAddress);
			$('.street_city', form).text(data.agency.city);
			$('.street_state', form).text(data.agency.state);
			$('.street_zip', form).text(data.agency.zip);
			$('.agency-entity', form).text(entityName);
			$('#accept-initial', form).attr("placeholder",customerName);
		};
		var addUserToDisplay = function(account_user){
			// console.log("Placeholder addUserToDisplay");
			//Reusable function on page load and on contact addition to add a contact's info to the DOM.
			//First, if we came from the add contact screen, let's clear our the fields and show a success message so they can quickly add a new contact.
			if (account_user.success===1) {
				$('#personnel_popup_form .wrapper').prepend('<div class="alert alert-success">\
					  <div class="alert-message">\
					      The contact has been added successfully\
					  </div>\
					</div>'); 
			} else{
 				$('#personnel_popup_form .wrapper').prepend('<div class="alert alert-error">\
				  <div class="alert-message">\
				      Your contact could not be saved. ' + account_user.message +
				  '</div>\
				</div>');  
 			}
	
			$("<p>" + account_user.first_name + " " + account_user.last_name + " - " + account_user.phone + " - " + account_user.email + " -  " + account_user.role + "</p> <!--<button personnel-id=" + account_user.id +" class='edit_personnel' class='button account-button-primary'>[EDIT]</button>-->").insertAfter(".authorized-contact h2");


			$("#personnel_popup_form input[name='addcontact-first_name']").val('');
			$("#personnel_popup_form input[name='addcontact-last_name']").val('');
			$("#personnel_popup_form input[name='addcontact-phone']").val('');
			$("#personnel_popup_form input[name='addcontact-email']").val('');
			$("#personnel_popup_form input[name='addcontact-role']").val('');


		}

		var setupAccountDetails = function() {
			$('.account-settings-form[action*=update_account]').wrap('<div id="account" class="content-section" />');
		};

		var showSection = function(sectionName) {
			log.debug('Displaying section: ' + sectionName);

			$('.content-section').fadeOut(100, function() {
				$('.content-section.visible').addClass('hidden');
				$('.content-section#' + sectionName).removeClass('hidden').fadeIn(100);
			});
		};

		this.init = function(logInstance) {
			log = logInstance;
			wrapper = $('.account-content');

			log.debug('Initializing customer administration section.');
			$.when(loadStylesheet(), 
			getAdminData()
				.pipe(setupAdminData)
				.pipe(getAdminTemplate)
				.pipe(setupAdminDisplay)
				.pipe(setupAccountDetails)
				.pipe(setupMenuLinks));
				// .pipe(function(){
				// 	showSection('account');
				// });
		};
	};

	if (!mbc.Safariland) mbc.Safariland = {};
	mbc.Safariland.CustomerAdmin = CustomerAdmin;
})(window.jQuery, MINIBC);