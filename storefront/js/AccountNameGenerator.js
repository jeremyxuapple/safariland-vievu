/**
 * Safariland Checkout Form
 *
 * @external {Object} jQuery
 * @external MINIBC
 */
if (!MINIBC) var MINIBC = {};

(function(mbc) {
	var AccountNameGenerator = function () {
		/**
		 * types of agency
		 *
		 * @type {[*]}
		 */
		var agencyTypes = [
			{'regex': new RegExp('marshal.s[ ]*?office', 'i'), 'abbreviation': 'MO'},
			{'regex': new RegExp('sheriff.s[ ]*?office', 'i'), 'abbreviation': 'SO'},
			{'regex': new RegExp('sheriff.s[ ]*?department', 'i'), 'abbreviation': 'SD'},
			{'regex': new RegExp('township[ ]*?police[ ]*?department', 'i'), 'abbreviation': 'TPD'},
			{'regex': new RegExp('college[ ]*?police[ ]*?department', 'i'), 'abbreviation': 'CPD'},
			{'regex': new RegExp('police[ ]*?department', 'i'), 'abbreviation': 'PD'},
			{'regex': new RegExp('port[ ]*?authority[ ]*?police', 'i'), 'abbreviation': 'PAP'},
			{'regex': new RegExp('state[ ]*?park[ ]*?police', 'i'), 'abbreviation': 'SPP'},
			{'regex': new RegExp('university[ ]*?police', 'i'), 'abbreviation': 'UP'},
			{'regex': new RegExp('university[ ]*?campus[ ]*?police', 'i'), 'abbreviation': 'UCP'},
			{'regex': new RegExp('campus[ ]*?police', 'i'), 'abbreviation': 'CP'},
			{'regex': new RegExp('police', 'i'), 'abbreviation': 'P'},
			{'regex': new RegExp('department[ ]*?of[ ]*?public[ ]*?safety', 'i'), 'abbreviation': 'DPP'},
			{'regex': new RegExp('airport[ ]*?authority', 'i'), 'abbreviation': 'AA'},
			{'regex': new RegExp('corrections', 'i'), 'abbreviation': 'C'}
		];

		/**
		 * types of university
		 *
		 * @type {[*]}
		 */
		var universityTypes = [
			{'regex': new RegExp('state[ ]*?university[ ]*?of', 'i'), 'abbreviation': 'SU'},
			{'regex': new RegExp('university[ ]*?of', 'i'), 'abbreviation': 'U'}
		];

		/**
		 * generate account name
		 *
		 * @param {String} legalName	legal name of the agency
		 * @param {String} stateISO2 	state in ISO2 format
		 * @return {*}
		 */
		this.generate = function(legalName, stateISO2) {
			if (!legalName) return '';
			var i, type, testResult;

			for (i = 0; i < universityTypes.length; i++) {
				type = universityTypes[i];
				testResult = legalName.match(type.regex);

				if (!testResult) continue;

				legalName = legalName.replace(testResult[0], type.abbreviation);
				break;
			}

			for (i = 0; i < agencyTypes.length; i++) {
				type = agencyTypes[i];
				testResult = legalName.match(type.regex);

				if (!testResult) continue;

				legalName = legalName.replace(testResult[0], type.abbreviation);
				break;
			}

			// remove spaces and return name
			legalName = legalName.replace(/ +/g, '');

			if (stateISO2) {
				legalName += '-' + stateISO2;
			}

			return legalName;
		};
	};

	/**
	 * assign to namespace
	 */
	if (!mbc.Safariland) {
		mbc.Safariland = {};
	}

	mbc.Safariland.AccountNameGenerator = AccountNameGenerator;
})(MINIBC);