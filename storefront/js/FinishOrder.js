// /registration/complete
if (!MINIBC) MINIBC = {};

(function($, mbc) {
	var FinishOrder = function() {
		var log;
		var apiBasePath = '/apps/sladministration/storefront';

		this.init = function(logInstance) {
			log = logInstance;

			// check for cookie
			var accountId = get_cookie('SL_PENDING_ID');

			if (accountId) {
				mbc.request({ 'account_id': accountId }, apiBasePath + '/registration/complete', 'POST', 'json');
			}
		};
	};

	if (!mbc.Safariland) mbc.Safariland = {};
	mbc.Safariland.FinishOrder = FinishOrder;
})(window.jQuery, MINIBC);