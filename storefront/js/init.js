/**
 * init app logic
 *
 * @external {Object} MINIBC
 * @property {Object} Safariland
 */
(function(CheckoutForm, CustomerAdmin, FinishOrder) {
	var uri = window.location.pathname;
	var search = window.location.search;
	var clientId = 'bangnjdpa5diyhjgq2hk8n7rodh43i';

	// setup logger
	var log = console;//new MINIBC.Log();
	//log.setAppName('sladministration');
	//log.setVerbosity('debug');

	if (uri.indexOf('/checkout.php') > -1) {
		// check logged in status
		MINIBC.getCustomerToken(clientId)
			.fail(function() {
				// not logged in
				log.debug('Failed to get customer token, customer is not logged in.');

				var checkoutForm = new CheckoutForm();
				checkoutForm.init(log);
			});
	}
	// console.log("sladministrationSL Chceking uri");
	if (uri.indexOf('/account.php') > -1 && search.indexOf('account_details') > -1) {
		// log.log("sladministrationSL Checking status...");
		// check logged in status
		MINIBC.getCustomerToken(clientId)
			.done(function() {
				var customerAdmin = new CustomerAdmin();
				customerAdmin.init(log);
			});
	}

	if (uri.indexOf('/finishorder.php') > -1) {
		var finishOrder = new FinishOrder();
		finishOrder.init(log);
	}
})(MINIBC.Safariland.CheckoutForm, MINIBC.Safariland.CustomerAdmin, MINIBC.Safariland.FinishOrder);