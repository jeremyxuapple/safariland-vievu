/**
 * Safariland Administration
 */
EmberApp.Router.map(function() {
	this.resource('sladministration', { path: '/apps/sladministration' }, function() {
        
        this.route('accounts', { path: '/accounts' }, function() {
        	this.route('account', { path: '/:id' });
        });

        this.route('reports', { path: '/reports' }, function() {

        });


	});
});

/**
 * Adapter for Safariland Administration
 *
 * @class SlAPIAdapter
 * @extends DS.RESTAdapter
 * @namspace EmberApp
 */
EmberApp.SlAPIAdapter = DS.RESTAdapter.extend({
    namespace: 'customer/apps/sladministration'
});