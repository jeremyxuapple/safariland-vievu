EmberApp.SlAuthorizedContact = DS.Model.extend({
    'account_id': DS.attr('number'),
    'first_name': DS.attr('string'),
    'last_name': DS.attr('string'),
    'email': DS.attr('string'),
    'phone': DS.attr('string'),
    'role': DS.attr('string')
});

EmberApp.SlAuthorizedContactAdapter = EmberApp.SlAPIAdapter.extend({});