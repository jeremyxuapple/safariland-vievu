/**
 * Safariland Administration
 *
 * @class Sladministration
 * @namespace EmberApp
 * @extends Ember.Route
 */
EmberApp.SladministrationRoute = Ember.Route.extend({

    /**
     * retrieve application information from the data store
     *
     * @function model
     * @returns {RSVP.Promise}
     */

    model: function() {
    	return Ember.RSVP.all([
    		this.store.peekRecord('app', 'sladministration')
    	]);
    },

    afterModel:function(model) {

    	var applicationController = this.controllerFor('application');
    },

    setupController: function(controller, model){
    	this._super(model, controller);

    	var app = model.objectAt(0);
    	var menuItems = appMenuItems.get('sladministration');

    	controller
    		.set('app', app)
    		.set('appsMenu', menuItems);


    },

    actions: {
    	'clearSearch': function() {
    		 this.controller.send('clearSearch');
    	}
    }


});