EmberApp.SladministrationAccountsAccountRoute = Ember.Route.extend({
    'model': function(params) {
    	return Ember.RSVP.all([
            this.store.findRecord('sl-account', params.id, { reload: true }),
            this.store.query('sl-authorized-contact', {account_id: params.id})
        ]);
    },

    'setupController': function(controller, model) {
    	controller.set('account', model.objectAt(0));
    	controller.set('authorizedContacts', model.objectAt(1));
    },

});