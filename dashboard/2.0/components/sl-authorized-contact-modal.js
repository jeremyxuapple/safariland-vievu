EmberApp.SlAuthorizedContactModalComponent = Ember.Component.extend({
    'tagName': 'div',
    'classNames': ['remodal'],
    'isInit': true,
    'closeAction': false,

    //Component lifecycle method
    'didInsertElement': function(){

        var inst = this.$().remodal({
            hashTracking: false,
            closeOnOutsideClick: true
        });

        inst.open();
        
        var component = this;
        
        this.$().on('closed', function() {
            component.sendAction('closeAction');
        }).on('opening', function() {
            component.set('isInit', true);
        }).on('opened', function() {
            component.set('isInit', false);
        });
        
    },

    'willDestroyElement': function() {
        var inst = this.$().remodal();
        if (inst) inst.destroy();
    },

    'actions': {

        'addAuthorizedContact': function(){
            var component = this;
            store = this.get('store');
            notifications = this.get('notifications');

            var account_id = this.get('account_id');

            // Authorized Contact Detail
            var addAuthorizeContact_first_name = $('#addAuthorizeContact_first_name').val();
            var addAuthorizeContact_last_name = $('#addAuthorizeContact_last_name').val();
            var addAuthorizeContact_email = $('#addAuthorizeContact_email').val();
            var addAuthorizeContact_phone = $('#addAuthorizeContact_phone').val();
            var addAuthorizeContact_role = $('#addAuthorizeContact_role').val();

            if(addAuthorizeContact_first_name === '' || addAuthorizeContact_last_name === ''){
                notifications.show('First name and last name cannot be blank','Failed','error');
                return;
            }

            var data = {
                'account_id': account_id,
                'first_name' : addAuthorizeContact_first_name,
                'last_name' : addAuthorizeContact_last_name,
                'email' : addAuthorizeContact_email,
                'phone' : addAuthorizeContact_phone,
                'role' : addAuthorizeContact_role
            };

            var authorizedContact = store.createRecord('sl-authorized-contact', data);

            authorizedContact.save().then(function(authorizedContact) {
                    notifications.show('Authorized contact has been successfully added','Success','success');
                    component.sendAction('closeAction');
                }, function(err) {
                    authorizedContact.rollback();
                    notifications.show('Failed to create authorized contact','Error','error');
                });
        },

        'updateAuthorizedContact': function(id){
            var component = this;
            store = this.get('store');
            notifications = this.get('notifications');
            var account_id = this.get('account_id');

            // Authorized Contact Detail
            var addAuthorizeContact_first_name = $('#addAuthorizeContact_first_name').val();
            var addAuthorizeContact_last_name = $('#addAuthorizeContact_last_name').val();
            var addAuthorizeContact_email = $('#addAuthorizeContact_email').val();
            var addAuthorizeContact_phone = $('#addAuthorizeContact_phone').val();
            var addAuthorizeContact_role = $('#addAuthorizeContact_role').val();

            store.findRecord('sl-authorized-contact', id).then(function(authorizedContact) {
                authorizedContact.set('account_id', account_id);
                authorizedContact.set('first_name', addAuthorizeContact_first_name);
                authorizedContact.set('last_name', addAuthorizeContact_last_name);
                authorizedContact.set('email', addAuthorizeContact_email);
                authorizedContact.set('phone', addAuthorizeContact_phone);
                authorizedContact.set('role', addAuthorizeContact_role);

                authorizedContact.save().then(function(authorizedContact){
                    notifications.show('Authorized contact has been successfully updated','Success','success');
                    component.sendAction('closeAction');
                }, function(err) {
                    console.log(err);
                    console.log('error');
                    authorizedContact.rollback();
                    notifications.show('Failed to update authorized contact','Error','error');
                });

            });
        },

        'exit': function(){
            this.sendAction('closeAction');
        }
    }


});