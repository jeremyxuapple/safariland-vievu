EmberApp.SlAccountListItemComponent = Ember.Component.extend({
	'tagName': '',
    'widget': false,

    // button actions
    'enableAction': false,
    'disableAction': false,
    'selectAction': false,
    'index': 0,
    'total': 1,

    'zIndex': function() {
		var total = this.get('total'),
			index = this.get('index');

		return parseInt(total) - parseInt(index);
    }.property('index', 'total'),

    'actions': {


    }

});