EmberApp.SladministrationReportsController = Ember.Controller.extend({
    'app': Ember.inject.controller('sladministration'),

    'actions':{
    	'createReport': function(reportType){

    		switch(reportType) {
            
            case 'creditCardExpiryReport':
               var start_date = $('#creditCardExpiryStartDate .datepicker').val();
               var end_date = $('#creditCardExpiryEndDate .datepicker').val();
               break;
            
            case 'billingReport':
               var start_date = $('#billingStartDate .datepicker').val();
               var end_date = $('#billingEndDate .datepicker').val();
               break;
            
            case 'accountReport':
               var start_date = $('#accountStartDate .datepicker').val();
               var end_date = $('#accountEndDate .datepicker').val();
               break;
            
            default:
               return;
         }

         var controller = this;

         var startDateFormat = new Date(start_date).getTime() / 1000;
         var endDateFormat = new Date(end_date).getTime() / 1000;

        if(start_date === '' || end_date === ''){
            this.notifications.show('Start and end date cannot be blank','Failed','error');
            return;
         }

        var data = {
                     'report_type': reportType,
                     'start_date': startDateFormat,
                     'end_date': endDateFormat
                  };

        Ember.$.ajax({
                        'url': '/customer/apps/sladministration/reports',
                        'type': 'POST',
                        'dataType': 'text',
                        'data': data,
                     }).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                           controller.notifications.show('Download will start soon.', 'Success', 'success');
                           var url = "data:text/csv,"+encodeURIComponent(resp);
                           var a = $("<a />", {
                              href: url,
                              download: reportType + ".csv"
                           })
                           .appendTo("body")
                           .get(0)
                           .click();
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.', 'Error', 'error');
                     });
    	},
    }

});