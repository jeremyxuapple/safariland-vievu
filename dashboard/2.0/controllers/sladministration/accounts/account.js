EmberApp.SladministrationAccountsAccountController = Ember.Controller.extend({
    'app': Ember.inject.controller('sladministration'),
    'refresh': true,

	'validationQuestions': 	[
		{id : 0, label : "What was the name of your elementary school?"},
		{id : 1, label : "What was the name of your first pet?"},
		{id : 2, label : "Who is your favorite superhero?"},
		{id : 3, label : "Who is your favorite author?"}
	],

	'governmentAgencies':[
		{id : 'federal', label : "Federal Government"},
		{id : 'local', label : "Local Government"},
		{id : 'state', label : "State Government"},
		{id : 'tribal', label : "Tribal Government"}
	],

	'accountStatus': [
		{id : 'not_confirmed', label : "Not Confirmed"},
		{id: 'submitted', label: "Submitted"},
		{id : 'confirmed', label : "Completed"},
		{id : 'abandoned', label : "Abandoned"},
		{id: 'failed', label: "Failed"},
		{id: 'cancelled', label: "Cancelled"}
	],

    'authorizedPurchaser': [
		{id : 0, label : "Yes"},
		// {id : 1, label : "No"}
	],

    'accountTypes': [
		{id: 'government', label: 'Government'},
		{id: 'public', label: 'Personal Use'}
	],

   	'selectOptions': { 'width': '100%', 'placeholder': 'Choose options ...' },

   	'accountWatcher': function(){
		var account = this.get('account');
   		var validationQuestionValue = {id : this.get('account.security_validation_question')};
   		var governmentAgencyValue = {id : this.get('account.signup_government_agency')};
   		var accountStatusValue = {id : this.get('account.account_status')};
   		var authorizedPurchaserValue = {id : 0};
   		var accountTypeValue = {id : this.get('account.signup_account_type')};

   		this.set('validationQuestionValue', validationQuestionValue);
   		this.set('governmentAgencyValue', governmentAgencyValue);
   		this.set('accountStatusValue', accountStatusValue);
   		this.set('authorizedPurchaserValue', authorizedPurchaserValue);
   		this.set('accountTypeValue', accountTypeValue);
   	}.observes('account'),

    'actions':{
    	'deleteAuthorizedContact': function(id){
    		var controller = this;

			this.store.findRecord('SlAuthorizedContact', id, { backgroundReload: false }).then(function(authorizedContacts) {
			  	authorizedContacts.deleteRecord();
			  	authorizedContacts.save()
					.then(function(authorizedContacts) {
						controller.notifications.show('Authorized contact deleted successfully.','Success','success');
					}, function(err) {
						  authorizedContacts.rollback();
						  controller.notifications.show('Failed to delete authorized contact.', 'Error', 'error');
					});
			});
    	},

		'viewAuthorizedContact': function(authorizedContact) {
			var contactInfo = {};
			contactInfo.id = authorizedContact.get('id');
			contactInfo.first_name = authorizedContact.get('first_name');
			contactInfo.last_name = authorizedContact.get('last_name');
			contactInfo.phone = authorizedContact.get('phone');
			contactInfo.email = authorizedContact.get('email');
			contactInfo.role = authorizedContact.get('role');

			this.set('showAuthorizedContactModal', true);
			this.set('viewAuthorizedContactInfo', contactInfo);
		},

		'showAuthorizedContact': function(){
			this.set('showAuthorizedContactModal', true);
			this.set('viewAuthorizedContactInfo', '');
		},

		'closeModal': function() {
			var controller = this;
			var account_id = this.get('account.id');
			var slAuthorizedContacts = this.store.query('sl-authorized-contact', {account_id: account_id});
			controller.set('authorizedContacts', slAuthorizedContacts);
			this.set('showAuthorizedContactModal', false);
		},

		'save': function(){
			var controller = this;
			var account = this.get('account');

			// Personal Detail
			var primary_first_name = $('#primary_first_name').val();
			var primary_last_name = $('#primary_last_name').val();
			var primary_phone = $('#primary_phone').val();
			var primary_role = $('#primary_role').val();

			// Administration Information
			var admin_first_name = $('#admin_first_name').val();
			var admin_last_name = $('#admin_last_name').val();
			var admin_phone = $('#admin_phone').val();
			var admin_role = $('#admin_role').val();

			// Security Information
			var security_email = $('#security_email').val();
			var security_username = $('#security_username').val();
			var security_validation_question = JSON.stringify(this.get('validationQuestionValue'));
			var security_validation_answer = $('#security_validation_answer').val();

			// Signup Information
			var signup_account_type = JSON.stringify(this.get('accountTypeValue'));
			var signup_government_agency = JSON.stringify(this.get('governmentAgencyValue'));
			var signup_authorized_purchaser = JSON.stringify(this.get('authorizedPurchaserValue'));
			var signup_account_status = JSON.stringify(this.get('accountStatusValue'));

			// Account
			var account_notes = $('#account_notes').val();

			account.set('primary_first_name', primary_first_name);
			account.set('primary_last_name', primary_last_name);
			account.set('primary_phone', primary_phone);
			account.set('primary_role', primary_role);
			account.set('admin_first_name', admin_first_name);
			account.set('admin_last_name', admin_last_name);
			account.set('admin_phone', admin_phone);
			account.set('admin_role', admin_role);
			account.set('security_email', security_email);
			account.set('security_username', security_username);
			account.set('security_validation_question', security_validation_question);
			account.set('security_validation_answer', security_validation_answer);
			account.set('signup_account_type', signup_account_type);
			account.set('signup_government_agency', signup_government_agency);
			account.set('signup_authorized_purchaser', signup_authorized_purchaser);
			account.set('signup_account_status', signup_account_status);
			account.set('account_notes', account_notes);

			account.save().then(function(account) {
				controller.notifications.show('Account successfully updated.', 'Success', 'success');
			}, function(err) {
				// error saving
				account.rollback();
				var message = 'An error occurred when updating this account. Please try again later.';
				if (err.errors !== undefined && err.errors.length > 0) {
					message = err.errors[0];
				}
				controller.notifications.show(
					message,
					'Failed to Update Account',
					'error'
				);
			});
		},

		'exit': function() {
			this.transitionTo('sladministration.accounts.index');
		}
    }
});