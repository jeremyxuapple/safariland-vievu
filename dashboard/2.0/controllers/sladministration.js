/**
 * Safariland Administration
 *
 * @class SafarilandController
 * @namespace EmberApp
 * @extends Ember.Controller
 */
EmberApp.SladministrationController = Ember.Controller.extend({
    'applicationCtrl': Ember.inject.controller('application')
});