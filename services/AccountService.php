<?php
namespace MiniBC\addons\sladministration\services;

use MiniBC\addons\sladministration\objects\Account;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Store;
use MiniBC\core\exceptions\NotFoundException;

class AccountService
{
	/**
	 * returns the account details
	 *
	 * @param int $accountId
	 * @param Store $store
	 * @return Account
	 * @throws NotFoundException
	 */
	public function getAccount($accountId, Store $store)
	{
		$accountId = (int)$accountId;

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$result = $db->selectFirst('sl_agency_account', array( 'id' => $accountId ));

		if (empty($result)) {
			throw new NotFoundException('Failed to find account for account ID ' . $accountId . '.');
		}

		$account = new Account();
		$account->id = $accountId;
		$account->store = $store;
		$account->name = $result['account_name'];
		$account->agency = new \stdClass;
		$account->agency->name = $result['agency_name'];
		$account->adminEmail = $result['admin_email'];
		$account->username = $result['username'];
		$account->password = $result['password'];

		return $account;
	}
}