<?php
namespace MiniBC\addons\sladministration\services;

use MiniBC\addons\bread\client\exceptions\ConnectionException;
use MiniBC\addons\sladministration\objects\Account;
use MiniBC\core\exceptions\GenericException;

class ApiService
{
	protected $adTenant = '';
	protected $host = '';
	protected $resource = '';
	protected $clientId = '';
	protected $clientSecret = '';
	protected $domain = '';

	/**
	 * ApiService constructor.
	 *
	 * @param string $resource
	 * @param string $clientId
	 * @param string $clientSecret
	 * @param string $allowedDomain
	 * @param string $adTenant
	 * @param string $apiHost
	 */
	public function __construct($resource, $clientId, $clientSecret, $allowedDomain, $adTenant, $apiHost)
	{
		$this->resource = $resource;
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
		$this->domain = $allowedDomain;

		$this->adTenant = $adTenant;
		$this->host = $apiHost;
	}

	/**
	 * get account status
	 *
	 * @param Account $account
	 * @return string returns account status
	 * @throws ConnectionException
	 * @throws GenericException
	 */
	public function getAccountStatus(Account $account)
	{
		$access = $this->getAccessToken();
		$action = 'tenants/' . $account->name . $this->domain . '/status';

		$header = array();
		$header[] = 'Authorization: ' . $access->token_type . ' ' . $access->access_token;

		$curl = curl_init($this->host . $action);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

		$response = curl_exec($curl);
		$curlErrorCode = curl_errno($curl);
		$httpResponseCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
		curl_close($curl);

		// connection error
		if ($curlErrorCode) {
			$error_message = curl_strerror($curlErrorCode);
			throw new ConnectionException($error_message, $curlErrorCode);
		}

		// tenant doesn't exist
		if ($httpResponseCode == 404) {
			return 'Failed';
		}

		// api error
		if ($httpResponseCode != 200) {
			$message = 'Failed to get tenant status from RMAPI.';
			if (isset($response->Message)) $message = $response->Message;

			throw new GenericException($message, $httpResponseCode);
		}

		$status = (!empty($response)) ? $response : '';

		return str_replace('"', '', $status);
	}

	/**
	 * create account in RM-API
	 *
	 * @param Account $account
	 * @return bool
	 * @throws ConnectionException
	 * @throws GenericException
	 */
	public function createAccount(Account $account)
	{
		$access = $this->getAccessToken();

		$agency = array(
			'Name' => $account->name . $this->domain,
			'Email' => $account->adminEmail,
			'Account' => array(
				'Email' => $account->adminEmail,
				'Username' => $account->username,
				'Password' => $account->password
			),
			'Licenses' => array(
				'Cameras' => 1,
				'Storage' => 50
			),
		);

		$json_encoded_tenants = json_encode($agency,JSON_FORCE_OBJECT);
		$action = 'tenants';

		$header = array();
		$header[] = 'Content-length: '.strlen($json_encoded_tenants);
		$header[] = 'Content-type: application/json';
		$header[] = 'Authorization: '.$access->token_type.' '.$access->access_token;
		$curl = curl_init($this->host . $action);

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_encoded_tenants);

		// Make the REST call, returning the result
		$response = curl_exec($curl);
		$curlErrorCode = curl_errno($curl);
		$httpResponseCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
		curl_close($curl);

		// connection error
		if ($curlErrorCode) {
			$error_message = curl_strerror($curlErrorCode);
			throw new ConnectionException($error_message, $curlErrorCode);
		}

		// tenant already exists
		if ($httpResponseCode == 409) {
			return true;
		}

		// api error
		if ($httpResponseCode != 200) {
			$message = 'Failed to create tenant in RMAPI.';
			if (isset($response->Message)) $message = $response->Message;

			throw new GenericException($message, $httpResponseCode);
		}

		return true;
	}

	/**
	 * get access token for API
	 *
	 * @return \stdClass
	 */
	private function getAccessToken()
	{
		$data = array(
			'grant_type' 	=> 'client_credentials',
			'resource' 		=> $this->resource,
			'client_id' 	=> $this->clientId,
			'client_secret' => $this->clientSecret
		);

		$postData = http_build_query($data);

		$url = "https://login.windows.net/{$this->adTenant}/oauth2/token";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		return json_decode($response);
	}
}