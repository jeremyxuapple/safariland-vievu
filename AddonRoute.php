<?php
namespace MiniBC\addons\sladministration;

use MiniBC\core\Auth;
use MiniBC\core\controller\ControllerManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\route\Route;
use MiniBC\core\Config;

class AddonRoute extends Route
{
	public $name = '';
	public $addon = null;

	protected $basePath = '';
	protected $customerPath;
	/**
	 * constructor
	 *
	 * @param Addon $addon
	 * @throws \Exception
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 */
	public function __construct(Addon $addon)
	{
		$this->name = $addon->name;
		$this->addon = $addon;

        $customer = Auth::getInstance()->getCustomer();
        $store = $customer->stores[0];

		// setup paths
		$customerBasePath = Config::get('routes::customer');
		$addonBasePath = Config::get('routes::addon');

		$this->basePath = $addonBasePath . '/' . $this->name;
		$this->customerPath = $customerBasePath . $this->basePath;

		$this->setAccountsRoute($store);
		$this->setReportsRoute($store);
		$this->setStorefrontRoute($store);
		$this->setVievuRoute($store);
	}

	private function setAccountsRoute(Store $store)
	{
		/** @var controllers\AccountController $controller */
		$controller = ControllerManager::get('Account@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);

		$accountsResourcePath = $this->customerPath . '/slAccounts';
		$this->get($accountsResourcePath, array($controller, 'getAccounts'));
		$this->put($accountsResourcePath . '/{id:.+}', array($controller, 'updateAccount'));
		$this->delete($accountsResourcePath . '/{id:.+}', array($controller, 'deleteAccount'));
		$this->get($accountsResourcePath . '/{id:.+}', array($controller, 'getAccount'));

		$authorizedContactResourcePath = $this->customerPath . '/slAuthorizedContacts';
		$this->get($authorizedContactResourcePath, array($controller, 'getAuthorizedContacts'));
		$this->post($authorizedContactResourcePath, array($controller, 'createAuthorizedContact'));
		$this->put($authorizedContactResourcePath . '/{id:.+}', array($controller, 'updateAuthorizedContact'));
		$this->delete($authorizedContactResourcePath . '/{id:.+}', array($controller, 'deleteAuthorizedContact'));
	}

	private function setReportsRoute(Store $store)
	{
		/** @var controllers\ReportsController $controller */
		$controller = ControllerManager::get('Reports@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);

		$this->post($this->customerPath . '/reports', array($controller, 'getReports'));
	}

	/**
	 * set routes to be accessible from the Bigcommerce storefront
	 *
	 * @param Store $store
	 */
	private function setStorefrontRoute(Store $store)
	{
		/** @var controllers\RegistrationController $registration */
		$registration = ControllerManager::get('Registration@' . $this->name);
		$registration->setAddon($this->addon);
		$registration->setStore($store);

		/** @var controllers\CustomerAdminController $admin */
		$admin = ControllerManager::get('CustomerAdmin@' . $this->name);
		$admin->setAddon($this->addon);
		$admin->setStore($store);

		$basePath = $this->basePath . '/storefront';

		$this->get($basePath . '/account/template', array($admin, 'getTemplate'));
		$this->post($basePath . '/account/adduser', array($admin, 'saveUser'));
		$this->post($basePath . '/account/data', array($admin, 'saveData'));
		$this->get($basePath . '/account/data', array($admin, 'getData'));
		$this->post($basePath . '/account/agency', array($registration, 'getAccountDetails'));
		$this->get($basePath . '/css', array($registration, 'loadStylesheet'));
		$this->get($basePath . '/registration/load', array($registration, 'loadTemplate'));
		$this->post($basePath . '/registration/save', array($registration, 'createRegistration'));
		$this->post($basePath . '/registration/complete', array($registration, 'completeRegistration'));
		$this->post($basePath . '/registration/cart/validate', array($registration, 'validateCart'));
	}

	private function setVievuRoute(Store $store)
	{
		/** @var controllers\VievuController $controller */
		$controller = ControllerManager::get('Vievu@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);

		$this->post($this->customerPath . '/slVievuAccount', array($controller, 'createVievuAccount'));
	}
}