<?php
namespace MiniBC\addons\sladministration\controllers;

use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\Auth;
use Symfony\Component\HttpFoundation\Response;

class CustomerAdminController
{
	/** @var Store $store */
	protected $store;

	/** @var Addon $addon */
	protected $addon;

	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

	public function getTemplate()
	{
		$path = __DIR__ . '/../storefront/templates/admin.html';

		if (!file_exists($path)) {
			return Response::create('', Response::HTTP_NOT_FOUND);
		}

		return Response::create(file_get_contents($path));
	}
	public function getData()
	{
		if (isset($_GET['email'])){
			$user_email = $_GET['email'];
			$customer = Auth::getInstance()->getCustomer();
	  		$db = ConnectionManager::getInstance('mysql');
			$agency_account = $db->selectFirst('sl_agency_account',array('customer_id'=>$customer->id, 'admin_email'=>$user_email));
			if ($agency_account!==false){
				$admin_contact = $db->selectFirst('sl_account_contact',array('customer_id'=>$customer->id, 'id'=>$agency_account['admin_contact_id']));
				$primary_contact = $db->selectFirst('sl_account_contact',array('customer_id'=>$customer->id, 'id'=>$agency_account['primary_contact_id']));
				$account_users = $db->select('sl_account_users',array('customer_id'=>$customer->id, 'account_id'=>$agency_account['id']));
				return Response::create(json_encode(array("agency_account"=>$agency_account, "admin_contact"=>$admin_contact, "primary_contact"=>$primary_contact, "account_users"=>$account_users)));
			}else{
				echo "Agency not found matching email ".$user_email;
				return Response::create('', Response::HTTP_BAD_REQUEST);	
			}
		}
		return Response::create('', Response::HTTP_BAD_REQUEST);		
	}

	public function saveData()
	{
		// var_dump($_POST);
		$agency_account_id = $_POST['agency_account_id'];
		$agency_account_agency_name = $_POST['agency_account_agency_name'];
		$agency_account_abbreviation = $_POST['agency_account_abbreviation'];
		$agency_account_address_1 = $_POST['agency_account_address_1'];
		$agency_account_address_2 = $_POST['agency_account_address_2'];
		$agency_account_city = $_POST['agency_account_city'];
		$agency_account_zip = $_POST['agency_account_zip'];
		$agency_account_state = $_POST['agency_account_state'];
		$agency_account_account_name = $_POST['agency_account_account_name'];
		$primary_contact_first_name = $_POST['primary_contact_first_name'];
		$primary_contact_last_name = $_POST['primary_contact_last_name'];
		$primary_contact_phone = $_POST['primary_contact_phone'];
		$primary_contact_role = $_POST['primary_contact_role'];
		$admin_contact_first_name = $_POST['admin_contact_first_name'];
		$admin_contact_last_name = $_POST['admin_contact_last_name'];
		$admin_contact_phone = $_POST['admin_contact_phone'];
		$admin_contact_role = $_POST['admin_contact_role'];
		$agency_account_admin_email = $_POST['agency_account_admin_email'];
		$agency_account_username = $_POST['agency_account_username'];
		$agency_account_question = $_POST['agency_account_question'];
		$agency_account_answer = $_POST['agency_account_answer'];

		$admin_contact_id = $_POST['admin_contact_id'];
		$primary_contact_id = $_POST['primary_contact_id'];

		if ($agency_account_id == '' || $agency_account_agency_name == '' || $agency_account_abbreviation == '' || $agency_account_address_1 == ''|| $agency_account_city == ''|| $agency_account_zip == ''|| $agency_account_state == ''|| $agency_account_account_name == ''|| $primary_contact_first_name == ''|| $primary_contact_last_name == ''|| $primary_contact_phone == ''|| $primary_contact_role == ''|| $admin_contact_first_name == ''|| $admin_contact_last_name == ''|| $admin_contact_phone == ''|| $admin_contact_role == ''|| $agency_account_admin_email == ''|| $agency_account_username == ''|| $agency_account_question == ''|| $agency_account_answer == ''){
			//echo "Blank request fields: ".$agency_account_id;
			return Response::create(json_encode(array("success"=>0, "message"=>"Please fill out all fields.")));		
		}

		$customer = Auth::getInstance()->getCustomer();
  		$db = ConnectionManager::getInstance('mysql');
		$agency_account_update = $db->update('sl_agency_account',array(
			'agency_name'=>$agency_account_agency_name,
			'address_1' => $agency_account_address_1,
			'address_2'=>$agency_account_address_2,
			'city'=> $agency_account_city,
			'zip' => $agency_account_zip,
			'state'=> $agency_account_state,
			'question'=>$agency_account_question,
			'answer'=>$agency_account_answer
		),array('id'=>$agency_account_id));

		if ($agency_account_update!==false){
			$admin_contact_update = $db->update('sl_account_contact',array(
					'first_name'=>$admin_contact_first_name,
					'last_name' => $admin_contact_last_name,
					'phone'=>$admin_contact_phone,
					'role'=> $admin_contact_role
				),array('id'=>$admin_contact_id));
			if ($admin_contact_update!==false){
				$primary_contact_update = $db->update('sl_account_contact',array(
					'first_name'=>$primary_contact_first_name,
					'last_name' => $primary_contact_last_name,
					'phone'=>$primary_contact_phone,
					'role'=> $primary_contact_role
				),array('id'=>$primary_contact_id));
				if ($primary_contact_update!==false){
					return Response::create(json_encode(array("success"=>1)));
				}else{
					echo "Primary contact could not be updated: ".$primary_contact_id;
					return Response::create('', Response::HTTP_BAD_REQUEST);	
				}
			}else{
				echo "Admin contact could not be updated: ".$admin_contact_id;
				return Response::create('', Response::HTTP_BAD_REQUEST);	
			}
		}else{
			echo "Agency could not be updated: ".$agency_account_id;
			return Response::create('', Response::HTTP_BAD_REQUEST);	
		}
		return Response::create('', Response::HTTP_BAD_REQUEST);		
	}

	public function saveUser()
	{
		// var_dump($_POST);
		$agency_account_id = $_POST['agency_account_id'];
		$user_first_name = $_POST['user_first_name'];
		$user_last_name = $_POST['user_last_name'];
		$user_email = $_POST['user_email'];
		$user_phone = $_POST['user_phone'];
		$user_role = $_POST['user_role'];
		if ($agency_account_id == '' || $user_first_name == ''|| $user_last_name == ''|| $user_phone == ''|| $user_role == ''|| $user_email == ''){
			return Response::create(json_encode(array("success"=>0, "message"=>"Please fill out all fields.")));		
		}			

		$customer = Auth::getInstance()->getCustomer();
  		$db = ConnectionManager::getInstance('mysql');
  		$now = time();
		$agency_account_update = $db->insert('sl_account_users',array(
			'customer_id'=>$customer->id,
			'account_id'=>$agency_account_id,
			'first_name'=>$user_first_name,
			'last_name' => $user_last_name,
			'email'=> $user_email,
			'phone'=>$user_phone,
			'role'=> $user_role,
			'create_time'=>$now
		));
		if ($agency_account_update!==false){
			return Response::create(json_encode(array("success"=>1,
												"id"=>$agency_account_update,
												"first_name"=>$user_first_name,
												"last_name"=>$user_last_name,
												"email"=>$user_email,
												"phone"=>$user_phone,
												"role"=>$user_role
		)));
		}else{
			echo "Conact could not be created: ".$agency_account_id;
			return Response::create('', Response::HTTP_BAD_REQUEST);	
		}
	}
}