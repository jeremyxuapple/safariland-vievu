<?php

namespace MiniBC\addons\sladministration\controllers;

use MiniBC\core\Auth;
use MiniBC\core\EntityFactory;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;

class ReportsController
{
	/** @var MySQLConnection $db */
	protected $db;

	/** @var Store $store */
	protected $store;

	/** @var Addon $addon */
	protected $addon;

	/** @var ReportsController $instance */
	private static $instance;

	# Customer Id
	protected $customerId;

	public function __construct()
	{
		$this->db = ConnectionManager::getInstance('mysql');

		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];

		$this->customerId = $customer->id;
		
		//$storeId = $store->id;

	}

	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function getReports()
	{
		$customer = Auth::getInstance()->getCustomer();
		$customer_store_id = $customer->id;
		$store = $customer->stores[0];

		$reportType = $_POST['report_type'];
		$startDate = $_POST['start_date'];
		$endDate = $_POST['end_date'];

		// $accountStatus = array(
		// 	'0' => 'Not Confirmed',
		// 	'1' => 'Confirmed',
		// 	'2' => 'Abandoned'
		// );

		// $accountTypes = array(
		// 	'0' => 'Agency',
		// 	'1' => 'Individual'
		// );

		$where = array(
			'customer_store_id' => $customer_store_id
		);
		$header=array();
		if ($reportType == 'creditCardExpiryReport') {

			$query = '
				SELECT saa.`agency_name`, saa.`account_name`, 
				CONCAT(sac.`first_name`, " ", sac.`last_name`) AS contact_name, 
				sac.`phone`, saa.`admin_email`, saa.`status`,
				FROM_UNIXTIME(saa.`create_time`) AS create_time, 
				rs.`next_payment`, rce.`expiry_date`	
				FROM sl_agency_account saa
				LEFT JOIN sl_account_contact sac 
					ON saa.`admin_contact_id` = sac.`id` 
					AND saa.`customer_id` = sac.`customer_id`
				LEFT JOIN rc_users ru
					ON ru.`store_customer_email` = saa.`admin_email`
					AND ru.`customer_id` = saa.`customer_id`
				LEFT JOIN rc_subscriptions rs
					ON rs.`rc_user_id` = ru.`id`
					AND rs.`customer_id` = saa.`customer_id`
				INNER JOIN rc_cc_expiry rce
					ON rs.`id` = rce.`subscription_id`
				WHERE saa.`customer_id` = :customer_store_id
				GROUP BY ru.`id`
			';

			$rows = $this->db->query($query, $where);

			if (empty($rows)) {
				$response = array('success' => false);
				echo json_encode($response);
				http_response_code(200);
				exit;
			} 
			
			$header = array('Agency Name', 'Account Name', 'Contact Name', 'Contact Phone', 'Contact Email', 'Status', 'Account Originating Date', 'Next Billing Date', 'Credit Card Expiry Date');
			
		}

		if ($reportType == 'billingReport') {

			$query = '
			 	SELECT saa.`agency_name`, saa.`account_name`, 
				CONCAT(sac.`first_name`, " ", sac.`last_name`) AS contact_name, 
				sac.`phone`, saa.`admin_email`, saa.`status`,
				saa.`status`,
				SUM( CONCAT(rs.`total`) ) AS billing_amount,
				FROM_UNIXTIME(saa.`create_time`) AS create_time, 
				rs.`next_payment`	
				FROM sl_agency_account saa
				LEFT JOIN sl_account_contact sac 
					ON saa.`admin_contact_id` = sac.`id` 
					AND saa.`customer_id` = sac.`customer_id`
				LEFT JOIN rc_users ru
					ON ru.`store_customer_email` = saa.`admin_email`
					AND ru.`customer_id` = saa.`customer_id`
				LEFT JOIN rc_subscriptions rs
					ON rs.`rc_user_id` = ru.`id`
					AND rs.`customer_id` = saa.`customer_id`
				WHERE saa.`customer_id` = :customer_store_id
				GROUP BY ru.`id`
			';

			$rows = $this->db->query($query, $where);
			if (empty($rows)) {
				$response = array('success' => false);
				echo json_encode($response);
				http_response_code(200);
				exit;
			} 
			
			$header = array('Agency Name', 'Account Name', 'Contact Name', 'Contact Phone', 'Contact Email', 'Status', 'Billing Amount', 'Account Originating Date', 'Next Billing Date');
		}

		if ($reportType == 'accountReport') {

			$query ='
				SELECT saa.`agency_name`, saa.`account_name`, 
				saa.`status`, 
				saa.`usage` AS licence_type, 
				SUM( CONCAT(rs.`product_quantity`) ) AS qty_license,
				bcop.`name` AS product_name,
				FROM_UNIXTIME(saa.`create_time`) AS create_time, 
				rs.`next_payment`
				FROM sl_agency_account saa
				LEFT JOIN sl_account_contact sac 
					ON saa.`admin_contact_id` = sac.`id` 
					AND saa.`customer_id` = sac.`customer_id`
				LEFT JOIN rc_users ru
					ON ru.`store_customer_email` = saa.`admin_email`
					AND ru.`customer_id` = saa.`customer_id`
				LEFT JOIN rc_subscriptions rs
					ON rs.`rc_user_id` = ru.`id`
					AND rs.`customer_id` = saa.`customer_id`
				INNER JOIN bigbackup_bc_orders_products bcop
					ON bcop.`bc_id` = rs.`order_product_id`
					AND bcop.`order_id` = rs.`order_id`
					AND bcop.`customer_id` = saa.`customer_id`
				WHERE saa.`customer_id` = :customer_store_id
				GROUP BY ru.`id`
			';

			$rows = $this->db->query($query, $where);

			if (empty($rows)) {
				$response = array('success' => false);
				echo json_encode($response);
				http_response_code(200);
				exit;
			}
	
			$header = array('Agency Name', 'Account Name', 'Status', 'License Type', 'License Product', 'Qty Licences', 'Account Originating Date', 'Next Billing Date');
		}

		// send file header
		header("Content-Type: text/csv;charset=utf-8");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

		// write CSV to output stream
		$output = fopen("php://output", "w");

		// write header
		fputcsv($output, $header);

		foreach ($rows as $row) {
			fputcsv($output, $row); // here you can change delimiter/enclosure
		}

		fclose($output);
		exit;

	}

}