<?php

namespace MiniBC\addons\sladministration\controllers;

use MiniBC\core\Auth;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController
{
	/** @var MySQLConnection $db */
	protected $db;

	/** @var Store $store */
	protected $store;

	/** @var Addon $addon */
	protected $addon;

	# Customer Id
	protected $customerId;

	public function __construct()
	{
		$this->db = ConnectionManager::getInstance('mysql');
	}

	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

	public function getAccounts()
	{
		$pages = 0;
		$pageNum = 1;
		$pageLimit = 10;
		$term = null;

		if (!empty($_GET)) {
			# pagination parameters
			if (!empty($_GET['page'])) {
				$pageNum = (int)$_GET['page'];
			}
			if (!empty($_GET['limit'])) {
				$pageLimit = (int)$_GET['limit'];
			}

			# search term
			if (!empty($_GET['term'])) {
				$term = strtolower($_GET['term']);
			}
		}

		$query = '
		SELECT %s 
		FROM `sl_agency_account` account 
		LEFT JOIN `sl_account_contact` contact 
			ON contact.`id` = account.`primary_contact_id` AND contact.`customer_id` = account.`customer_id`
		LEFT JOIN `sl_account_contact` admin
			ON admin.`id` = account.`admin_contact_id` AND admin.`customer_id` = account.`customer_id`
		WHERE account.`customer_id` = :customer_id AND ( %s ) ORDER BY account.`create_time` DESC
		';

		$where = '1 = 1';

		# Pagination
		if (!empty($term)) {
			$where = "
			account.`agency_name` LIKE '%$term%'
			OR contact.`first_name` LIKE '%term%'
			OR contact.`last_name` LIKE '%term%'
			";
		}

		$countQuery = sprintf($query, 'COUNT(account.`id`) AS total', $where);
		$countResult = $this->db->queryFirst($countQuery, array( ':customer_id' => $this->store->id ));

		$total = (int)$countResult['total'];

		if ($total > 1) {
			# find total number of cells
			$pages = ceil($total / $pageLimit);
		}

		# Query
		$fields = '
		account.`id`,
		account.`create_time` AS account_signup_date,
		account.`agency_name`,
		account.`abbreviation` AS agency_abbreviation,
		account.`address_1` AS agency_address_1,
		account.`address_2` AS agency_address_2,
		account.`city` AS agency_city,
		account.`state` AS agency_state,
		account.`zip` AS agency_zip,
		account.`admin_email` AS security_email,
		account.`username` AS security_username,
		account.`password` AS security_password,
		account.`question` AS security_validation_question,
		account.`answer` AS security_validation_answer,
		account.`entity` AS signup_government_agency,
		account.`usage` AS signup_account_type,
		account.`notes` AS account_notes,
		account.`status` AS account_status,
		contact.`first_name` AS primary_first_name,
		contact.`last_name` AS primary_last_name,
		contact.`phone` AS primary_phone,
		contact.`role` AS primary_role,
		admin.`first_name` AS admin_first_name,
		admin.`last_name` AS admin_last_name,
		account.`account_name` AS account_name,
		admin.`phone` AS admin_phone,
		admin.`role` AS admin_role
		';
		$selectQuery = sprintf($query, $fields, $where);

		if ($pageLimit) {
			$selectQuery .= "LIMIT $pageLimit ";
		}

		if ($pageNum > 1) {
			$selectQuery .= "OFFSET " . $pageLimit * ($pageNum - 1);
		}

		$data = $this->db->query($selectQuery, array( ':customer_id' => $this->store->id ));

		if (empty($data)) {
			// var_dump($this->db->getPDOException());
		}

		$meta = array('pages' => $pages, 'page' => $pageNum, "term" => $term, 'limit' => $pageLimit);
		$response = array('slAccounts' => $data, 'meta' => $meta);

		echo json_encode($response);
		exit;
	}

	public function getAccount($id)
	{
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];
		$account_id = $id;

		# Query
		$query = "
		SELECT 
			account.`id`,
			account.`create_time` AS account_signup_date,
			account.`agency_name`,
			account.`abbreviation` AS agency_abbreviation,
			account.`address_1` AS agency_address_1,
			account.`address_2` AS agency_address_2,
			account.`city` AS agency_city,
			account.`state` AS agency_state,
			account.`zip` AS agency_zip,
			account.`admin_email` AS security_email,
			account.`username` AS security_username,
			account.`password` AS security_password,
			account.`question` AS security_validation_question,
			account.`answer` AS security_validation_answer,
			account.`entity` AS signup_government_agency,
			account.`usage` AS signup_account_type,
			account.`notes` AS account_notes,
			account.`status` AS account_status,
			contact.`first_name` AS primary_first_name,
			contact.`last_name` AS primary_last_name,
			contact.`phone` AS primary_phone,
			contact.`role` AS primary_role,
			admin.`first_name` AS admin_first_name,
			admin.`last_name` AS admin_last_name,
			account.`account_name` AS account_name,
			admin.`phone` AS admin_phone,
			admin.`role` AS admin_role	
		FROM `sl_agency_account` account 
		LEFT JOIN `sl_account_contact` contact 
			ON contact.`id` = account.`primary_contact_id` AND contact.`customer_id` = account.`customer_id`
		LEFT JOIN `sl_account_contact` admin
			ON admin.`id` = account.`admin_contact_id` AND admin.`customer_id` = account.`customer_id`
		WHERE account.`id` = :account_id AND account.`customer_id` = :customer_id
		";

		$results = $this->db->query($query, array( ':account_id' => $id, ':customer_id' => $this->store->id ));

		$response = array('slAccounts' => $results);
		echo json_encode($response);
		exit;
	}

	public function updateAccount($args = array())
	{
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];
		$account_id = $args['id'];
		$data = $_POST['slAccount'];

		$primary_first_name = $data['primary_first_name'];
		$primary_last_name = $data['primary_last_name'];
		$primary_phone = $data['primary_phone'];
		$primary_role = $data['primary_role'];
		$admin_first_name = $data['admin_first_name'];
		$admin_last_name = $data['admin_last_name'];
		$admin_phone = $data['admin_phone'];
		$admin_role = $data['admin_role'];
		$security_email = $data['security_email'];
		$security_username = $data['security_username'];
		$security_validation_question = json_decode($data['security_validation_question'])->id;
		$security_validation_answer = $data['security_validation_answer'];
		$signup_account_type = json_decode($data['signup_account_type'])->id;
		$signup_government_agency = json_decode($data['signup_government_agency'])->id;
		$signup_authorized_purchaser = json_decode($data['signup_authorized_purchaser'])->id;
		$signup_account_status = json_decode($data['signup_account_status'])->id;
		$account_notes = $data['account_notes'];

		$query = "
		UPDATE sl_agency_account account
		LEFT JOIN sl_account_contact admin ON account.admin_contact_id = admin.id
		LEFT JOIN sl_account_contact contact on account.primary_contact_id = contact.id
		SET 
		contact.first_name = '$primary_first_name', 
		contact.last_name = '$primary_last_name', 
		contact.phone = '$primary_phone', 
		contact.role = '$primary_role',
		account.admin_email = '$security_email',
		account.username = '$security_username', 
		account.question = '$security_validation_question', 
		account.answer = '$security_validation_answer', 
		account.entity = '$signup_account_type',
		account.usage = '$signup_government_agency',
		account.status = '$signup_account_status',
		admin.first_name = '$admin_first_name', 
		admin.last_name = '$admin_last_name', 
		admin.phone = '$admin_phone', 
		admin.role = '$admin_role', 
		account.notes = '$account_notes'
		WHERE account.customer_id = $store->id AND account.id = $account_id
		";

		$results = $this->db->execute($query);

		if ($results === false) {
			return Response::create('', Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		$response = array('success' => true, 'id' => $args['id'], 'data' => $data);

		return JsonResponse::create($response);
	}

	public function getAuthorizedContacts(Request $request)
	{
		$accountId = (int)$request->query->get('account_id', 0);
		$contacts = array();

		if ($accountId > 0) {
			$query = "
			SELECT `id`, `first_name`, `last_name`, `email`, `phone`, `role`
			FROM `sl_account_users`
			WHERE `customer_id` = :customer_id AND `account_id` = :account_id
			";

			$results = $this->db->query($query, array( ':customer_id' => $this->store->id, ':account_id' => $accountId ));

			if (!empty($results)) {
				$contacts = $results;
			}
		}

		return JsonResponse::create(array( 'slAuthorizedContacts' => $contacts ));
	}

	public function deleteAuthorizedContact($id)
	{
		$response = array( 'success' => true );
		$responseCode = Response::HTTP_OK;

		$results = $this->db->delete('sl_account_users', array( 'id' => $id, 'customer_id' => $this->store->id ));

		if ($results === false) {
			$response['success'] = false;
			$responseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
		}

		return JsonResponse::create($response, $responseCode);
	}

	public function createAuthorizedContact()
	{
		$data = $_POST['slAuthorizedContact'];

		$insertToDb = array(
			'customer_id' 	=> $this->store->id,
			'account_id' 	=> $data['account_id'],
			'first_name' 	=> $data['first_name'],
			'last_name'  	=> $data['last_name'],
			'email'      	=> $data['email'],
			'phone'      	=> $data['phone'],
			'role'       	=> $data['role'],
			'create_time'	=> time()
		);

		$userId = $this->db->insert('sl_account_users', $insertToDb);

		// failed to save to database
		if ($userId === false) {
			return JsonResponse::create(array( 'success' => false ), Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		return JsonResponse::create(array( 'success' => true ), Response::HTTP_OK);
	}

	public function updateAuthorizedContact($id, Request $request)
	{
		$data = $request->request->get('slAuthorizedContact', false);
		$result = $this->db->update('sl_account_users', $data, array('id' => $id));

		// failed to save to database
		if ($result === false) {
			return JsonResponse::create(array( 'success' => false ), Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		return JsonResponse::create(array( 'success' => true ));
	}
}