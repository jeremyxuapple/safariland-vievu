<?php

namespace MiniBC\addons\sladministration\controllers;

use MiniBC\addons\sladministration\services\AccountService;
use MiniBC\addons\sladministration\services\ApiService;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\exceptions\GenericException;
use MiniBC\core\exceptions\NotFoundException;
use MiniBC\core\Log;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController
{
	/** @var MySQLConnection $db */
    protected $db;

    /** @var Store $store */
    protected $store;

    /** @var Addon $addon */
    protected $addon;

	# Account id
	protected $accountId;

	public function __construct()
	{
		$this->db = ConnectionManager::getInstance('mysql');
	}

	public function setStore(Store $store)
    {
        $this->store = $store;
    }

    public function setAddon(Addon $addon)
    {
        $this->addon = $addon;
    }

	/**
	 * load html template for registration form
	 *
	 * @return Response
	 */
    public function loadTemplate()
	{
		$path = __DIR__ . '/../storefront/templates/popup.html';

		if (!file_exists($path)) {
			return Response::create('', Response::HTTP_NOT_FOUND);
		}

		return Response::create(file_get_contents($path));
	}

	/**
	 * load storefront CSS
	 *
	 * @return Response
	 */
	public function loadStylesheet()
	{
		$path = __DIR__ . '/../storefront/css/style.css';

		if (!file_exists($path)) {
			return Response::create('', Response::HTTP_NOT_FOUND);
		}

		return Response::create(file_get_contents($path));
	}

	public function getAccountDetails(Request $request)
	{
		$response = array('success' => true);
		$customerToken = $request->request->get('customer_token', false);

		if (empty($data)) {

		}
	}

	public function validateCart(Request $request)
	{
		// var_dump($request);
		// var_dump($request->request);
		var_dump($request->request->get('products'));
		var_dump($request->request->get('products',array()));
		$response = array('register' => true);
		$products = $request->request->get('products', array());
		//return JsonResponse::create($response);
		if (empty($products)) {
			$response['register'] = false;

			return JsonResponse::create($response);
		}

		echo "We have a product!";

		$products = array_map(function($productId) {
			return (int)$productId;
		}, $products);
		
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$results = $db->query(
			sprintf(
				'SELECT id FROM rc_products WHERE product_id IN (%s) AND customer_id = :customer_id',
				implode(',', $products)
			),
			array(':customer_id' => $this->store->id)
		);
		var_dump($results);

		if (empty($results)) {
			echo "Not a rc_products";
			$response['register'] = false;
		}

		return JsonResponse::create($response);
	}

	public function createRegistration(Request $request)
	{
		$response = array('success' => true);

		try {
			$accountId = $this->createAccount($request);
			$response['account_id'] = $accountId;
		} catch (\Exception $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}

		return JsonResponse::create($response);
	}

	public function completeRegistration(Request $request)
	{
		$response = array('success' => true);
		$accountId = (int)$request->request->get('account_id', 0);

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$setting = $db->selectFirst('sl_settings', array( 'customer_id' => $this->store->id ));

		try {
			$accountService = new AccountService();
			$account = $accountService->getAccount($accountId, $this->store);

			$api = new ApiService(
				$setting['resource'],
				$setting['client_id'],
				$setting['client_secret'],
				$setting['domain'],
				$setting['tenant'],
				$setting['host']
			);

			// create account in RM API
			$api->createAccount($account);

			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');

			// update status in database
			$db->update('sl_agency_account', array( 'status' => 'submitted' ), array( 'id' => $accountId ));
		} catch (NotFoundException $nf) {
			$response['success'] = false;
			$response['message'] = $nf->getMessage();
		} catch (\Exception $e) {
			$response['success'] = false;
			$response['message'] = $e->getMessage();

			Log::addError(
				'Failed to create account in RMAPI: ' . $e->getMessage(),
				array(
					'customer_id' => $this->store->id,
					'account_id' => $accountId,
					'code' => $e->getCode(),
					'message' => $e->getMessage(),
					'trace' => $e->getTraceAsString()
				)
			);
		}

		return JsonResponse::create($response);
	}

	/**
	 * create agency account
	 *
	 * @param Request $request
	 * @return int account ID
	 * @throws GenericException
	 */
	private function createAccount(Request $request)
	{
		$data = $request->request->all();

		if (empty($data['agency']) || empty($data['security'])) {
			throw new GenericException('Missing agency information.');
		}

		$defaultAgencyData = array(
			'customer_id'		=> $this->store->id,
			'agency_name'		=> '',
			'abbreviation' 		=> '',
			'address_1' 		=> '',
			'address_2' 		=> '',
			'city' 				=> '',
			'zip' 				=> '',
			'username'			=> '',
			'password'			=> '',
			'question'			=> '',
			'answer'			=> '',
			'primary_contact_id'=> 0,
			'admin_contact_id'	=> 0,
			'admin_email'		=> '',
			'entity'			=> '',
			'usage'				=> '',
			'create_time' 	=> time()
		);

		$agencyData = array_map('trim', $data['agency']);
		$agencyData = array_filter($agencyData, function($field) {
			return (!empty($field));
		});

		// create contacts
		if (!empty($data['admin'])) {
			$adminId = $this->createContact($data['admin']);
			$agencyData['admin_contact_id'] = $adminId;
		}

		if (!empty($data['contact'])) {
			$adminId = $this->createContact($data['contact']);
			$agencyData['primary_contact_id'] = $adminId;
		}

		$agencyData['entity'] = $data['entity'];
		$agencyData['usage'] = $data['usage'];

		$securityData = array_map('trim', $data['security']);
		$securityData = array_filter($securityData, function($field) {
			return (!empty($field));
		});

		$agencyData = array_merge($defaultAgencyData, $agencyData, $securityData);

		// check if account name already exists
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$exists = $db->select('sl_agency_account', array( 'account_name' => $agencyData['account_name'] ));

		if (!empty($exists)) {
			throw new GenericException('An account already exists for this agency.');
		}

		$accountId = $this->db->insert('sl_agency_account', $agencyData);

		if (!$accountId) {
			throw new GenericException('Failed to save agency data.');
		}

		return (int)$accountId;
	}

	private function createContact($contact)
	{
		$contact['customer_id'] = $this->store->id;
		$contact['create_time'] = time();

		return $this->db->insert('sl_account_contact', $contact);
	}
}
