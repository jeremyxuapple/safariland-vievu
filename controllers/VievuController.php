<?php
namespace MiniBC\addons\sladministration\controllers;

use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;

class VievuController
{
	/** @var MySQLConnection $db */
    protected $db;

    /** @var Store $store */
    protected $store;

    /** @var Addon $addon */
    protected $addon;

    /** @var VievuController $instance */
	private static $instance;

	# Customer Id
	protected $customerId;

	# Account id
	protected $accountId;

	# VIEVU credential to get access token
	protected $resourceKey;

	protected $clientIdKey;

	protected $clientSecretKey;

	protected $grantTypeKey;

	protected $agencyInfo;

	protected $result;


	public function __construct()
	{
		$this->db = ConnectionManager::getInstance('mysql');
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];

		$this->customerId = $customer->id;
		$this->customerId = 118;
		$this->resourceKey = 'a292a89e-8298-4d58-a2d7-b53ccf46deec';
		$this->clientIdKey = 'ae4cfe13-fb80-46ac-b5a4-748631f4e8d3';
		$this->clientSecretKey = '5VAfnxZsV6iq2QJyCeKgTT9Gq9L8QtcwaVn5jqAufKc=';
		$this->grantTypeKey = 'client_credentials';
		$this->accountId = 1;
	}

	public function consoleLog($var){
		echo '<pre>'.print_r($var,true).'</pre>';
	}

	public function setStore(Store $store)
    {
        $this->store = $store;
    }

    public function setAddon(Addon $addon)
    {
        $this->addon = $addon;
    }

	public static function getInstance()
	{
	
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function getAgencyInfo()
	{
		
		# Query
		$query = 
			"
			SELECT 
			sla.account_id as id,
			sla.signup_date as account_signup_date,
			sla.agency_name as agency_name, 
			sla.agency_abbreviation as agency_abbreviation, 
			sla.agency_address_1 as agency_address_1, 
			sla.agency_address_2 as agency_address_2, 
			sla.agency_city as agency_city, 
			sla.agency_zip as agency_zip, 
			sla.agency_state as agency_state, 
			slpi.first_name as primary_first_name, 
			slpi.last_name as primary_last_name, 
			slpi.phone as primary_phone, 
			slpi.role as primary_role,
			sls.administrator_email as security_email,
			sls.agency_username as security_username, 
			sls.agency_password as security_password,
			sls.validation_question as security_validation_question, 
			sls.validation_answer as security_validation_answer, 
			slsi.account_type as signup_account_type,
			slsi.government_agency as signup_government_agency,
			slsi.authorized_purchaser as signup_authorized_purchaser,
			slsi.account_status as signup_account_status,
			slai.first_name as admin_first_name, 
			slai.last_name as admin_last_name, 
			slai.phone as admin_phone, 
			slai.role as admin_role, 
			slan.notes as account_notes
			FROM sl_account_agency sla
			LEFT JOIN sl_account_primary_info slpi on slpi.account_id = sla.account_id
			LEFT JOIN sl_account_security sls on sls.account_id = sla.account_id
			LEFT JOIN sl_account_signup_info slsi on slsi.account_id = sla.account_id
			LEFT JOIN sl_account_admin_info slai on slai.account_id = sla.account_id
			LEFT JOIN sl_account_admin_notes slan on slan.account_id = sla.account_id
			WHERE sla.store_id = $this->customerId AND sla.account_id = $this->accountId
			";

		$this->agencyInfo= $this->db->query($query);
	}

	public function createVievuAccount($args = array())
	{
		$this->getAgencyInfo();
		$this->getAccessTokenVievuAPI();
		$this->createAccount();	
	}

	private function getAccessTokenVievuAPI()
	{
		$post_data="grant_type=".$this->grantTypeKey."&resource=".$this->resourceKey."&client_id=".$this->clientIdKey."&client_secret=".$this->clientSecretKey;
		$url="https://login.windows.net/vievudevelopmentoutlook.onmicrosoft.com/oauth2/token";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));   
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$response = curl_exec($ch);
		$this->result = json_decode($response);
	}

	private function createAccount()
	{
		$agency = [
					'Name' => str_replace(" ", "", $this->agencyInfo[0]['agency_name']),
					'Email' => $this->agencyInfo[0]['security_email'],
					'Account' => [
						'Email' => $this->agencyInfo[0]['security_email'],
						'Username' => $this->agencyInfo[0]['security_username'],
						'Password' => $this->agencyInfo[0]['security_password']
					],
					// 'Storage' => [
					// 	'Location' => '',
					// 	'DataRedundancy' => ''
					// ],
					'Licenses' => [
						'Cameras' => 1,
						'Storage' => 50
					]
		];

		$json_encoded_tenants = json_encode($agency,JSON_FORCE_OBJECT);
		$this->consoleLog(($json_encoded_tenants));
		//die;
		$url = 'https://rmapiinternal.cloudapp.net/api/';
		$action = 'tenants';
		
		$headr = array();
		$headr[] = 'Content-length: '.strlen($json_encoded_tenants);
		$headr[] = 'Content-type: application/json';
		$headr[] = 'Authorization: '.$this->result->token_type.' '.$this->result->access_token;
		$curl = curl_init($url . $action);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headr); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_encoded_tenants); 
		// Make the REST call, returning the result
		$response = curl_exec($curl);
		if($errno = curl_errno($curl)) {
    		$error_message = curl_strerror($errno);
 	 		echo "cURL error ({$errno}):\n {$error_message} \n";
		}else if (!curl_errno($curl)) {
  			$http_code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
  			if ($http_code==409){
  				print_r("Tenant already exists");// \r\n";
  			}
  			else if ($http_code!=200){
  				echo "HTTP Response Code ". $http_code . "\n";
  				if (isset($response->Message))echo $response->Message;
  			}else{
  				echo "Success.";
  			}
		}

		curl_close($curl);
	}

	private function updateAccountStatus()
	{
		$query = 'Update sl_account_signup_info set account_status=1 where account_id ='.$this->accountId;
		$this->db->execute($query);
	}

}