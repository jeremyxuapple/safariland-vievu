<?php
namespace MiniBC\addons\sladministration\commands;

use MiniBC\addons\sladministration\objects\Account;
use MiniBC\addons\sladministration\services\ApiService;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateAccountStatus extends Command
{
	/**
	 * configure command
	 */
	protected function configure()
	{
		$this
			->setName('sladministration:update-account-status')
			->setDescription('Check RMAPI and update account status in database.')
			->setDefinition(
				new InputDefinition(array(
					new InputOption('store_id', null, InputOption::VALUE_REQUIRED, 'Enter the store ID.')
				))
			);
	}

	/**
	 * execute command
	 *
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$storeId = (int)$input->getOption('store_id');

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		// get settings
		$setting = $db->selectFirst('sl_settings', array( 'customer_id' => $storeId ));

		$api = new ApiService(
			$setting['resource'],
			$setting['client_id'],
			$setting['client_secret'],
			$setting['domain'],
			$setting['tenant'],
			$setting['host']
		);

		$updateCount = 0;
		$failed = array();

		$io = new SymfonyStyle($input, $output);
		$io->title('Update Account Status from RM-API');

		$accounts = $db->query(
			'
			SELECT * 
			FROM `sl_agency_account`
			WHERE `status` IN ("submitted", "in_progress") 
				AND `customer_id` = :customer_id
			',
			array(':customer_id' => $storeId)
		);

		$totalAccounts = count($accounts);
		$io->text($totalAccounts . ' accounts found.');

		if ($totalAccounts > 0) {
			$io->newLine(2);
			$io->progressStart($totalAccounts);

			foreach ($accounts as $data) {
				$account = new Account();
				$account->id = (int)$data['id'];
				$account->name = $data['account_name'];

				try {
					$status = $api->getAccountStatus($account);
					$dbStatus = 'not_confirmed';

					if (strripos($status, 'progress') !== false) {
						$dbStatus = 'in_progress';
					} else if (strripos($status, 'failed') !== false) {
						$dbStatus = 'failed';
					} else if (strripos($status, 'completed') !== false) {
						$dbStatus = 'confirmed';
					}

					// update database with new account status
					$db->update(
						'sl_agency_account',
						array('status' => $dbStatus),
						array('id' => $account->id, 'customer_id' => $storeId)
					);
				} catch (\Exception $e) {
					// failed to retrieve shipping address from BC API
					$failed[] = array(
						'id'    => $account->id,
						'name'  => $account->name,
						'error' => $e->getMessage()
					);

					$io->progressAdvance(1);

					continue;
				}

				$updateCount++;
				$io->progressAdvance(1);
			}

			$io->progressFinish();
			$io->newLine(2);

			$this->generateSummary($updateCount, $failed, $io);
		}
	}

	/**
	 * generate summary
	 *
	 * @param int $updateCount	number of accounts updated
	 * @param array $failed		failed data
	 * @param SymfonyStyle $io	I/O formatter
	 */
	private function generateSummary($updateCount, $failed, SymfonyStyle $io)
	{
		$io->text($updateCount . ' accounts updated.');

		if (!empty($failed)) {
			$io->newLine(2);
			$io->section('Failures');
			$io->table( array( 'Account ID', 'Tenant Name', 'Error' ), $failed );
		}
	}
}